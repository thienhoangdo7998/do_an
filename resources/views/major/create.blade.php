@extends('layout.admin')

@section('module_user')

      <div class="content-header">

        <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>thêm Nghiệp Vụ</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Nghiệp Vụ</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
          <section class="content">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-12">

                        <div class="card">
                          <div class="card-header">
                            <h3 class="card-title">Thêm Thông Tin Nghiệp Vụ</h3>
                          </div>

                              <form role="form"  method="post" action="">
                                <div class="card-body">
                                  <div class="form-group">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <label>Tên Nghiệp Vụ</label>
                                    <div class="input-group mb-3" style="align-content: center;">
                                    <input type="text" name="ten_nghiep_vu" class="form-control" placeholder="Tên Nghiệp Vụ">
                                    <div class="input-group-append">
                                      <div class="input-group-text">
                                          <span class="fas fa-users"></span>
                                      </div>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="card-footer" >
                                    <button type="submit" class="btn btn-primary">Thêm Nghiệp Vụ</button>
                                </div>
                               
                              </form>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
          </section>
      </div>
  </div>

@endsection