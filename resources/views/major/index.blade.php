@extends('layout.admin')

@section('content')
 <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thông Tin Nghiệp Vụ</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Nghiệp Vụ</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
<div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{route('major.create')}}" class="btn btn-primary btn-flat" style="margin: 0px 0px 0px 10px;border-radius: 10px">
                        <i class="fas fa-plus"></i> {{ trans('Thêm Nghiệp Vụ') }}
                    </a>
</div>

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Danh Sách Nghiệp Vụ</h3>
                </div>
            <!-- /.card-header -->
                <div class=" card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Nghiệp Vụ</th>
                                    <th style="text-align: center; vertical-align: middle;">Xóa</th>
                                    <th style="text-align: center; vertical-align: middle;">Sửa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($majors)): ?>
                                    <?php $i = 1;?>
                                <?php foreach ($majors as $major): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$i++}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$major->Ten_Nghiep_Vu}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{route('major.delete',[$major->id])}}"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-block" href="{{route('major.edit',[$major->id])}}" > <i class="fas fa-pen"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
           </div>
        </div>
    </div>
</section> 
@stop


