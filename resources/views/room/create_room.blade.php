@extends('layout.admin')

@section('module_user')
<div class="content-header">

 
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thêm phòng ban</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin phòng ban</li>
                 </ol>
                </div>
              </div>
            </div>
 
    
          <section class="content">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-12">

                        <div class="card">
                          <div class="card-header">
                            <h3 class="card-title">Thêm Thông Tin phòng ban</h3>
                          </div>

                              <form role="form"  method="post" action="">
                                <div class="card-body">
                                  <div class="form-group">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                   

                                   <label>Tên Phòng Ban</label>
                                    <div class="input-group mb-3" style="align-content: center;">
                                    <input type="text" name="ten_pb" class="form-control" placeholder="tên phòng ban">
                                    <div class="input-group-append">
                                      <div class="input-group-text">
                                          <span class="fas fa-users"></span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                </div>

                                <div class="card-footer" >
                                    <button type="submit" class="btn btn-primary">Thêm phòng ban</button>
                                </div>
                               
                              </form>
                            </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
          </section>

          
@endsection
