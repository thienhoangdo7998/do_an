@extends('layout.admin')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Thông Tin Phòng Ban</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Thông Tin Phòng Ban</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
        <a href="{{route('create_room')}}" class="btn btn-primary btn-flat" style="margin: 0px 0px 0px 10px;border-radius: 10px">
            <i class="fas fa-plus"></i> {{ trans('Thêm phòng ban') }}
        </a>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Danh Sách Phòng Ban</h3>
            </div>
            <!-- /.card-header -->
            <div class=" card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" >
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle;">Id</th>
                                <th style="text-align: center; vertical-align: middle;">Tên Phòng Ban</th>
                                <th style="text-align: center; vertical-align: middle;">Xóa</th>
                                <th style="text-align: center; vertical-align: middle;">Sửa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($rooms)): ?>
                                <?php $i = 1;?>
                            <?php foreach ($rooms as $room): ?>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="{{route('room_listnv',[$room->id])}}">{{$i++}}</a>
                                    </td>

                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$room->Ten_PB}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <div class="btn-group">
                                            <a class="btn btn-danger" href="{{route('delete_room',[$room->id])}}"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <div class="btn-group">
                                            <a class="btn btn-primary btn-block" href="{{route('edit_room',[$room->id])}}"> <i class="fas fa-pen"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php endif; ?> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  
          


@stop


