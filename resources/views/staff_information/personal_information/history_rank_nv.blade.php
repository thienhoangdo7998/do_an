@extends('layout.admin')

@section('module_user')
<section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Thông Tin Nhân Viên {{$lsrank[0]->Ho_Ten}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item active">Thông Tin {{$lsrank[0]->Ho_Ten}}</li>
             </ol>
            </div>
          </div>
        </div>
</section>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">      
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Cấp Bậc</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngày Lên Hàm</th>
                                </tr>
                            </thead>
                            <tbody>                          
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@stop