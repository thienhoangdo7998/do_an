@extends('layout.admin')

@section('content')
<section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thông Tin Nhân Viên</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Nhân Viên</li>
                 </ol>
                </div>
              </div>
            </div>
</section>
<div class="container-fluid">
    <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                    <h3 class="card-title">Thông Tin Tìm Kiếm</h3>
              </div>
              <div class="card-body">
                    <div class="row">
                        <form method="POST" class="col-md-12" action="{{route('staff.find')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}";>
                            <div class="input-group input-group mb-3">
                                <input class="typeahead form-control" type="search" id="value" name="value" placeholder="Tìm Kiếm" autocomplete="off" aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>        
                        </form>
                    </div>

                    <form class="form-group col-12" method="POST" action="{{route('staff.filter')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}";>
                        <div class="row">
                            <div class="col-md-3">
                                <select class="select form-control" name="cap_bac" id="select2">
                                    <option value="" disabled selected>Hãy Chọn Cấp Bậc</option>
                                    <?php if(isset($ranks)): ?>
                                    <?php foreach ($ranks as $ds): ?>
                                        <option value="{{$ds->id}}">{{$ds->Ten_Cap_Bac}}</option>
                                    <?php endforeach; ?>     
                                    <?php endif; ?>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <select class="select form-control" name="Ly_Luan_Chinh_Tri" id="select2">
                                    <option value="" disabled selected>Hãy Chọn Lý Luận Chính Trị</option>
                                    <option value="Cao Cấp">Cao Cấp</option>
                                    <option value="Trung Cấp">Trung Cấp</option>
                                    <option value="Sơ Cấp">Sơ Cấp</option>
                                    <option value="Tương Đương">Tương Đương</option>
                                </select>  
                            </div>

                            <div class="col-md-3">
                                <select class="select form-control" name="Trinh_Do" id="select2">
                                    <option value="" disabled selected>Hãy Chọn Trình Độ</option>
                                    <?php if(isset($degrees)): ?>
                                    <?php foreach ($degrees as $ds): ?>
                                        <option value="{{$ds->id}}">{{$ds->Ten_trinh_do}}</option>
                                    <?php endforeach; ?>     
                                    <?php endif; ?>
                                </select>  

                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary btn">
                                    <i class="fas fa-search"></i> Bộ Lọc
                                </button>
                            </div>   
                        </div>
                     </form>
                  </div>
              <!-- /.card-body -->
                  <div class="card-footer">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-xl2">
                            Thông Tin Cán Bộ 2 Văn Bằng
                        </button>
                        <a href="{{route('xuat_excel')}}" class="btn btn-success"><i class="fas fa-plus-square"></i> Xuất Excel Thông Tin Nhân Viên</a>
                  </div>
              <!-- /.card-footer-->
            </div>
            <!-- /.card -->
          </div>
        </div>
    </div>

      <div class="modal fade" id="modal-xl2">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Thông Tin Cán Bộ 2 Văn Bằng</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-primary card-outline">
                                <div class="card-body pad table-responsive">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" >
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                                    <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                                    <th style="text-align: center; vertical-align: middle;">Văn Bằng Khác</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (isset($vanbang)): ?>
                                                <?php $i = 1;?>
                                                <?php foreach ($vanbang as $ds): ?>
                                                    <tr>
                                                        <td style="text-align: center; vertical-align: middle;">
                                                            <a href="#">{{$i++}}</a>
                                                        </td>

                                                        <td style="text-align: center; vertical-align: middle;">
                                                            <a href="#">{{$ds->Ho_Ten}}</a>
                                                        </td>
                                                        <td style="text-align: center; vertical-align: middle;">
                                                            <a href="#">{{$ds->Van_Bang_Khac}}</a> 
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                <?php endif; ?> 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

                      
<div class="row">  
    <div class="container-fluid">
        <div class="col-md-12" >
            <div class="card card-primary card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" personal_information="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" personal_information="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Thông Tin Nhân Viên</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" personal_information="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Trình Độ</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" personal_information="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Công Tác</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages1" personal_information="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Chức Vụ</a>
                  </li>
                </ul>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="btn-group pull-right" style="margin: 15px 15px 0px 20px;">
                        <a href="{{route('staff.create')}}" class="btn btn-primary btn" style="padding: 4px 10px;">
                            <i class="fas fa-plus"></i> {{ trans('Thêm Tài Khoản Người Dùng') }}
                        </a>
                    </div> 
                </div>
              </div>
              <div class="card-body">

                <div class="tab-content" id="custom-tabs-one-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" personal_information="tabpanel" aria-labelledby="custom-tabs-one-home-tab">

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                    <th style="text-align: center; vertical-align: middle;">Hình Ảnh</th>
                                    <th style="text-align: center; vertical-align: middle;">Giới Tính</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngày Sinh</th>
                                    <th style="text-align: center; vertical-align: middle;">Quê Quán</th>
                                    <th style="text-align: center; vertical-align: middle;">Dân Tộc</th>
                                    <th style="text-align: center; vertical-align: middle;">Tôn Giáo</th>
                                    <th style="text-align: center; vertical-align: middle;">Nơi Ở Hiện Tại</th>
                                    <th style="text-align: center; vertical-align: middle;">Xóa</th>
                                    <th style="text-align: center; vertical-align: middle;">Sửa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($personal_informations)): ?>
                                    <?php $i = 1;?>
                                <?php foreach ($personal_informations as $personal_information): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$i++}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="{{route('staff.hsl',[$personal_information->id])}}">{{$personal_information->Ho_Ten}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <img src="../adminlte/dist/img/{{$personal_information->Avatar}}" height="90px" width="90px" />
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Gioi_Tinh}}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Sinh)))}}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Que_Quan}}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Dan_Toc}}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Ton_Giao}}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Noi_O_Hien_Tai}}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{route('staff.delete',[$personal_information->id])}}"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-block" href="{{route('staff.edit',[$personal_information->id])}}" > <i class="fas fa-pen"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                  </div>

                <div class="tab-pane fade" id="custom-tabs-one-profile" personal_information="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                    <th style="text-align: center; vertical-align: middle;">Trình Độ</th>
                                    <th style="text-align: center; vertical-align: middle;">Lý Luận Chính Trị</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngoại Ngữ</th>
                                    <th style="text-align: center; vertical-align: middle;">Tin Học</th>
                                    <th style="text-align: center; vertical-align: middle;">Xóa</th>
                                    <th style="text-align: center; vertical-align: middle;">Sửa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($personal_informations)): ?>
                                    <?php $i = 1;?>
                                <?php foreach ($personal_informations as $personal_information): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$i++}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Ho_Ten}}</a>
                                        </td>
                                         <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Ten_trinh_do
                                            }}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Ly_Luan_Chinh_Tri}}</a> 
                                        </td>
                                         <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Ngoai_Ngu}}</a> 
                                        </td>
                                         <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Tin_Hoc}}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{route('staff.delete',[$personal_information->id])}}"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-block" href="{{route('staff.edit',[$personal_information->id])}}"> <i class="fas fa-pen"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                  <div class="tab-pane fade" id="custom-tabs-one-messages" personal_information="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                    <div class="table-responsive">
                     <table class="table table-bordered table-hover" >
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle;">STT</th>
                                <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                <th style="text-align: center; vertical-align: middle;">Ngày Vào Đoàn</th>
                                <th style="text-align: center; vertical-align: middle;">Ngày Vào Đảng</th>
                                <th style="text-align: center; vertical-align: middle;">Ngày Vào Làm</th>
                                <th style="text-align: center; vertical-align: middle;">Tuổi Đảng</th>
                                <th style="text-align: center; vertical-align: middle;">Xóa</th>
                                <th style="text-align: center; vertical-align: middle;">Sửa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($personal_informations)): ?>
                                <?php $i = 1;?>
                            <?php foreach ($personal_informations as $personal_information): ?>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$i++}}</a>
                                    </td>

                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$personal_information->Ho_Ten}}</a>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#"> {{date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Vao_Doan)))}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Vao_Dang)))}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Vao_Lam)))}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$bien-$personal_information->Tuoi_Dang}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <div class="btn-group">
                                            <a class="btn btn-danger" href="{{route('staff.delete',[$personal_information->id])}}"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <div class="btn-group">
                                            <a class="btn btn-primary btn-block" href="{{route('staff.edit',[$personal_information->id])}}" > <i class="fas fa-pen"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php endif; ?> 
                        </tbody>
                    </table>
                </div>
            </div>
                    <div class="tab-pane fade" id="custom-tabs-one-messages1" personal_information="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                    <div class="table-responsive">
                     <table class="table table-bordered table-hover" >
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle;">STT</th>
                                <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                <th style="text-align: center; vertical-align: middle;">Chức Vụ Đảng</th>
                                <th style="text-align: center; vertical-align: middle;">Chức Vụ Chính Quyền</th>
                                <th style="text-align: center; vertical-align: middle;">Chức Vụ Đoàn Thể</th>
                                <th style="text-align: center; vertical-align: middle;">Cấp Bậc</th>
                                <th style="text-align: center; vertical-align: middle;">Nghiệp Vụ</th>
                                <th style="text-align: center; vertical-align: middle;">Xóa</th>
                                <th style="text-align: center; vertical-align: middle;">Sửa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($personal_informations)): ?>
                                <?php $i = 1;?>
                            <?php foreach ($personal_informations as $personal_information): ?>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$i++}}</a>
                                    </td>

                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$personal_information->Ho_Ten}}</a>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$personal_information->TenChucvuDang}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$personal_information->TenChucvuCQ}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$personal_information->TenChucvuDoan}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$personal_information->Ten_Cap_Bac}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$personal_information->Ten_Nghiep_Vu}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <div class="btn-group">
                                            <a class="btn btn-danger" href="{{route('staff.delete',[$personal_information->id])}}"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <div class="btn-group">
                                            <a class="btn btn-primary btn-block" href="{{route('staff.edit',[$personal_information->id])}}" ><i class="fas fa-pen"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php endif; ?> 
                        </tbody>
                    </table>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
          </div>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

<script type="text/javascript">
    var path = "{{route('staff.autocomplete')}}";
    $('#value').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
    
</script>
@stop

