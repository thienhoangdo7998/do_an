@extends('layout.admin')

@section('module_user')
<section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thông Tin Nhân Viên Tăng Lương</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Nhân Viên Tăng Lương</li>
                 </ol>
                </div>
              </div>
            </div>
</section>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">      
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                    <th style="text-align: center; vertical-align: middle;">Hệ Số Lương</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngày Lên Lương</th>
                                    <th style="text-align: center; vertical-align: middle;">Xác Nhận Tăng Lương</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($personal_informations)): ?>
                                    <?php $i = 1;?>
                                <?php foreach ($personal_informations as $personal_information): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$i++}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Ho_Ten}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->He_So_Luong}}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$personal_information->Ngay_Len_Ham}}</a> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-block" href="{{route('staff.hsl1',[$personal_information->id])}}"><i class="fas fa-pen"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@stop