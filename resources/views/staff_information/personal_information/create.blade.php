@extends('layout.admin')

@section('module_user')
<section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thêm Thông Tin Nhân Viên</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Nhân Viên</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
<form role="form" id="quickForm" method="post" name="createform"  enctype="multipart/form-data" action="">
  <input type="hidden" name="_token" value="{{csrf_token()}}">
 
   <div class="container-fluid">
      <div class="row">
            <!-- left column -->
        <div class="col-md-12">
              <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Thêm Thông Tin Nhân Viên</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
              
              <div class="card-body">
                <div class="row">
                  
                    <div class="col-12 col-md-6">
                      <label>Họ Tên Nhân Viên</label>
                      <div class="input-group mb-6" style="align-content: center;">
                        <input type="text" name="Ho_Ten" class="form-control" placeholder="Họ Tên Nhân Viên">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-users"></span>
                            </div>
                        </div>
                      </div>  
                    </div>
                  
                     <div class="col-12 col-md-6">
                      <div class="row">
                        <label class="file"></label>
                        <input type="file" id="file-field" onchange="previewImage(event)" name="Avatar" aria-label="File browser example" style="margin: 35px 20px 20px 20px"><br>
                        <img src="{{asset('adminlte/dist/img/unnamed.jpg')}}" id="image-field" width="125" height="125" style="margin: 0px 20px 20px 20px;border-radius: 20px">
                        <span id="file-field" style="color:red;text-align: right;"></span>
                      </div>
                    </div>
                </div> 

                <div class="row">
                  <div class="col-12 col-md-6">
                    <label>Ngày Sinh</label>
                    <div class="input-group mb-3" >
                        <div class="input-group date" id="reservationdate" data-target-input="nearest">
                            <input type="text" placeholder="dd/mm/YYYY" name="Ngay_Sinh" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                      <label>Giới Tính</label>
                      <select  class="select form-control" name="Gioi_Tinh" id="select2">
                          <option value="" disabled selected>Hãy Giới Tính</option>
                          <option value="Nam">Nam</option>
                          <option value="Nữ">Nữ</option>
                      </select>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <label>Nơi Ở Hiện Tại</label>
                      <div class="input-group mb-3" style="align-content: center;">
                        <input type="text" name="Noi_O_Hien_Tai" class="form-control" placeholder="Nơi Ở Hiện Tại">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-users"></span>
                            </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                    <label>Email</label>
                    <div class="input-group mb-3">
                      <input type="email" name="email" class="form-control" placeholder="Email">
                      <div class="input-group-append">
                        <div class="input-group-text">
                          <span class="fas fa-envelope"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>  

                 <div class="row">
                   <div class="col-12 col-md-4">
                      <label>Quê Quán</label>
                      <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Que_Quan" class="form-control" placeholder="Quê quán">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-users"></span>
                            </div>
                        </div>
                      </div> 
                   </div>

                   <div class="col-12 col-md-4">
                      <label>Tôn Giáo</label>
                      <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ton_Giao" class="form-control" placeholder="Tôn Giáo">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-users"></span>
                            </div>
                        </div>
                      </div> 
                   </div>

                   <div class="col-12 col-md-4">
                      <label>Dân Tộc</label>
                      <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Dan_Toc" class="form-control" placeholder="Dân Tộc">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-users"></span>
                            </div>
                        </div>
                      </div> 
                   </div>
                  </div>
                  <div class="row">
                    <div class="col-12 col-md-6">
                      <label>Lý Luận Chính Trị</label>
                      <select  class="select form-control" name="Ly_Luan_Chinh_Tri" >
                          <option value="" disabled selected>Hãy Lý Luận Chính Trị</option>
                          <option value="Cao Cấp">Cao Cấp</option>
                          <option value="Trung Cấp">Trung Cấp</option>
                          <option value="Sơ Cấp">Sơ Cấp</option>
                          <option value="Tương Đương">Tương Đương</option>
                      </select>
                    </div>
                    <div class="col-12 col-md-6">
                      <label>Trình Độ</label>
                      <select  class="select form-control" onchange="ChangeCarList(this)" name="Trinh_Do" id="Trinh_Do">
                          <option value="" disabled selected>Hãy Chọn Trình Độ</option>
                          <?php if(isset($degree)): ?>
                          <?php foreach ($degree as $ds): ?>
                              <option value="{{$ds->id}}">{{$ds->Ten_trinh_do}}</option>
                          <?php endforeach; ?>     
                          <?php endif; ?> 
                      </select>
                    </div>
                  </div>
                 <div class="row">
                  <div class="col-12 col-md-6">
                    <label>Ngoại Ngữ</label>
                    <div class="input-group mb-3" style="align-content: center;">
                    <input type="text" name="Ngoai_Ngu" class="form-control" placeholder="Dân Tộc">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <label>Tin Học</label>
                    <div class="input-group mb-3" style="align-content: center;">
                    <input type="text" name="Tin_Hoc" class="form-control" placeholder="Dân Tộc">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                
               <div class="row">
                   <div class="col-12 col-md-4">
                    <label>Ngày Vào Đảng</label>
                    <div class="input-group mb-3">
                        <div class="input-group date"  id="reservationdate1" data-target-input="nearest">
                            <input type="text" placeholder="dd/mm/YYYY" name="Ngay_Vao_Dang" class="form-control datetimepicker-input" data-target="#reservationdate1"/>
                            <div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                  </div>

                  <div class="col-12 col-md-4">
                    <label>Ngày Vào Đoàn</label>
                    <div class="input-group mb-3">
                        <div class="input-group date"  id="reservationdate2" data-target-input="nearest">
                            <input type="text" placeholder="dd/mm/YYYY" name="Ngay_Vao_Doan" class="form-control datetimepicker-input" data-target="#reservationdate2"/>
                            <div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                  </div>

                  <div class="col-12 col-md-4">
                    <label>Ngày Vào Làm</label>
                    <div class="input-group mb-3">
                        <div class="input-group date"  id="reservationdate3" data-target-input="nearest">
                            <input type="text" placeholder="dd/mm/YYYY" name="Ngay_Vao_Lam" class="form-control datetimepicker-input" data-target="#reservationdate3"/>
                            <div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <label>Lương Cơ Bản</label>
                    <div class="input-group mb-3" style="align-content: center;">
                    <input type="text" name="Luong_CB" class="form-control" placeholder="Lương Cơ Bản">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <label>Văn Bằng Khác</label>
                    <div class="input-group mb-3" style="align-content: center;">
                    <input type="text" name="Van_Bang" class="form-control" placeholder="Văn Bằng Khác">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                   <div class="col-12 col-md-4">
                      <label>Chức Vụ Chính Quyền</label>
                      <select  class="select form-control" name="government_positions_id" >
                        <option value="" disabled selected>Hãy Chọn Chức Vụ Chính Quyền</option>
                        <?php if(isset($goverment_positions)): ?>
                        <?php foreach ($goverment_positions as $ds): ?>
                            <option value="{{$ds->id}}">{{$ds->TenChucvuCQ}}</option>
                        <?php endforeach; ?>     
                        <?php endif; ?> 
                      </select> 
                   </div>

                   <div class="col-12 col-md-4">
                      <label>chức Vụ Đoàn</label>
                      <select  class="select form-control" name="corporate_position_id" >
                        <option value="" disabled selected>Hãy Chọn Chức Vụ Đoàn</option>
                        <?php if(isset($corporate_positions)): ?>
                        <?php foreach ($corporate_positions as $ds): ?>
                            <option value="{{$ds->id}}">{{$ds->TenChucvuDoan}}</option>
                        <?php endforeach; ?>     
                        <?php endif; ?> 
                      </select> 
                   </div>

                   <div class="col-12 col-md-4">
                      <label>chức Vụ Đảng</label>
                      <select  class="select form-control" name="party_position_id" >
                        <option value="" disabled selected>Hãy Chọn Chức Vụ Đảng</option>
                        <?php if(isset($party_positions)): ?>
                        <?php foreach ($party_positions as $ds): ?>
                            <option value="{{$ds->id}}">{{$ds->TenChucvuDang}}</option>
                        <?php endforeach; ?>     
                        <?php endif; ?> 
                      </select> 
                   </div>
                </div>
             
                <div class="row">
                     <div class="col-12 col-md-4">
                        <label>Nghiệp Vụ</label>
                        <select  class="select form-control" name="major_id">
                          <option value="" disabled selected>Hãy Chọn Nghiệp Vụ</option>
                          <?php if(isset($majors)): ?>
                          <?php foreach ($majors as $ds): ?>
                              <option value="{{$ds->id}}">{{$ds->Ten_Nghiep_Vu}}</option>
                          <?php endforeach; ?>     
                          <?php endif; ?> 
                        </select> 
                     </div>

                     <div class="col-12 col-md-4">
                        <label>Cấp Bậc</label>
                        <select  class="select form-control" name="rank_id" id="rank_id">
                          <option value="" disabled selected>Hãy Chọn Cấp Bậc </option>
                        </select> 
                     </div>

                     <div class="col-12 col-md-4">
                       <label>Phòng Ban</label>
                       <select class="select form-control" name="room_id">
                         <option value="" disabled selected>Hãy Chọn Phòng Ban</option>
                          <?php if(isset($rooms)): ?>
                          <?php foreach ($rooms as $ds): ?>
                              <option value="{{$ds->id}}">{{$ds->Ten_PB}}</option>
                          <?php endforeach; ?>     
                          <?php endif; ?> 
                       </select>
                     </div>
                </div>
              </div>

             
              <!-- /.card-body -->
              </div>
      
            
        </div>
      </div>
    </div>

   @include('staff_information.family_information.create')
</form>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


<script>
  $(function () {

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $('#reservationdate1').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#reservationdate2').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#reservationdate3').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script type="text/javascript">
  function previewImage(){
    var reader = new FileReader();
    var imageField = document.getElementById("image-field");

    reader.onload = function(){
      if(reader.readyState == 2){
        imageField.src = reader.result;
      }
    }
    reader.readAsDataURL(event.target.files[0]);
  }
</script>
<script>
function ChangeCarList(obj) {
  var carList = document.getElementById("Trinh_Do");
  var modelList = document.getElementById("rank_id");
  var bien = obj.value;
  var bien1 = "";
  var ten = "";
  var i;
  while (modelList.options.length) {
    modelList.remove(0);
  }
  if (bien == 1) 
  {
    for (i = 1; i <= 12; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }
  else if(bien == 2)
  {
    for (i = 1; i <= 15; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }
  else if(bien == 3){
    for (i = 1; i <= 19; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }
  else if(bien == 4){
    for (i = 1; i <= 27; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }

  else if(bien == 5)
  {
    for (i = 1; i <= 35; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }

  else
  {
    for (i = 1; i <= 43; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }
} 
</script>

<script type="text/javascript">
$(document).ready(function () {
  $('#quickForm').validate({
    rules: {
      Ho_Ten: {
        required: true,
      },
      Avatar: {
        required: true,
      },
      Ngay_Sinh: {
        required: true
      },
      Ngay_Vao_Lam: {
        required: true
      },
      Ngay_Vao_Doan: {
        required: true
      },
      Ngay_Vao_Dang: {
        required: true
      },
      Gioi_Tinh: {
        required: true
      },
      Noi_O_Hien_Tai: {
        required: true
      },
      Que_Quan: {
        required: true
      },
      Dan_Toc: {
        required: true
      },
      Ly_Luan_Chinh_Tri: {
        required: true
      },
      Ngoai_Ngu: {
        required: true
      },
      Tin_Hoc: {
        required: true
      },
      room_id: {
        required: true
      },
      Tinh_Trang_Lam_Viec:{
         required: true
      },
      government_positions_id:{
         required: true
      },
      corporate_position_id:{
         required: true
      },
      rank_id:{
         required: true
      },
      party_position_id:{
         required: true
      },
      major_id:{
         required: true
      },
      Luong_CB:{
         required: true
      },
    },
    messages: {
       Ho_Ten: {
        required: "Chưa nhập họ tên"
      },
      Avatar: {
        required: "Chưa nhập họ tên"
      },
      Ngay_Sinh: {
        required: "Chưa nhập họ tên"
      },
      Ngay_Vao_Lam: {
       required: "Chưa nhập họ tên"
      },
      Ngay_Vao_Doan: {
        required: "Chưa nhập họ tên"
      },
      Ngay_Vao_Dang: {
        required: "Chưa nhập họ tên"
      },
      Gioi_Tinh: {
        required: "Chưa nhập họ tên"
      },
      Noi_O_Hien_Tai: {
        required: "Chưa nhập họ tên"
      },
      Que_Quan: {
        required: "Chưa nhập họ tên"
      },
      Dan_Toc: {
        required: "Chưa nhập họ tên"
      },
      Ly_Luan_Chinh_Tri: {
        required: "Chưa nhập họ tên"
      },
      Ngoai_Ngu: {
        required: "Chưa nhập họ tên"
      },
      Tin_Hoc: {
        required: "Chưa nhập họ tên"
      },
      room_id: {
        required: "Chưa nhập họ tên"
      },
      Tinh_Trang_Lam_Viec:{
         required: "Chưa nhập họ tên"
      },
      government_positions_id:{
         required: "Chưa nhập họ tên"
      },
      corporate_position_id:{
        required: "Chưa nhập họ tên"
      },
      rank_id:{
         required: "Chưa nhập họ tên",
      },
      party_position_id:{
         required: "Chưa nhập họ tên",
      },
      major_id:{
         required: "Chưa nhập họ tên",
      },
      Luong_CB:{
         required: "Chưa nhập họ tên",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.col').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>

@endsection
 