@extends('layout.admin')

@section('module_user')

<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Thông Tin Nhân Viên</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Thông Tin </li>
         </ol>
        </div>
      </div>
    </div>
</section>

 <div class="container-fluid">
    <div class="row">
          <div class="col-12">
                <div class="card">
                  <div class="card-header">
                        <h3 class="card-title">Thông Tin Tìm Kiếm</h3>
                  </div>
                  <div class="card-body">
                      <form class="form-group col-12" method="POST"  action="{{route('staff.find_ls_nv')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}";>
                        <div class="row">
                            <div class="col-md-3">
                                <select class="select form-control" name="Ho_Ten" id="select2">
                                    <option value="" disabled selected>Hãy Chọn Cán Bộ</option>
                                    <?php if(isset($personal_informations)): ?>
                                    <?php foreach ($personal_informations as $ds): ?>
                                        <option value="{{$ds->Ma_NV}}">{{$ds->Ho_Ten}}</option>
                                    <?php endforeach; ?>     
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary btn">
                                    <i class="fas fa-search"></i> Hiện Lịch Sử Cán Bộ
                                </button>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>        
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary ">
                <div class="card-header">
                    <h3 class="card-title">Lịch Sử Lên Hàm</h3>
                </div>
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                                <thead>
                                    <tr>
                                        <th style="text-align: center; vertical-align: middle;">STT</th>
                                        <th style="text-align: center; vertical-align: middle;">Họ Tên</th>
                                        <th style="text-align: center; vertical-align: middle;">Hình Ảnh</th>
                                        <th style="text-align: center; vertical-align: middle;">Cấp Bậc</th>
                                        <th style="text-align: center; vertical-align: middle;">Chức Vụ Đảng</th>
                                        <th style="text-align: center; vertical-align: middle;">Chức Vụ Đoàn</th>
                                        <th style="text-align: center; vertical-align: middle;">Chức Vụ Chính Quyền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($list)): ?>
                                    <?php $i = 1;?>
                                    <?php foreach ($list as $lists): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p>{{$i++}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$lists->Ho_Ten}}</p> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <img src="../../adminlte/dist/img/{{$lists->Avatar}}" height="90px" width="90px" />
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$lists->Ten_Cap_Bac}}</p> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$lists->TenChucvuDang}}</p> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$lists->TenChucvuDoan}}</p> 
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$lists->TenChucvuCQ}}</p> 
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?> 
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" id="abc">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary ">
                <div class="card-header">
                    <h3 class="card-title">Lịch Sử Lên Hàm</h3>
                </div>
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">      
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Cấp Bậc</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngày Lên Hàm</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($rank)): ?>
                                <?php $i = 1;?>
                                <?php foreach ($rank as $personal_information): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$i++}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$personal_information->Ten_Cap_Bac}}</p> 
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">
                                            {{date('d-m-Y',strtotime(str_replace('/','-',$personal_information->LS_Ngay_Tang_Ham)))}}</p> 
                                        </td>

                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6"> 
            <div class="card card-primary ">
                <div class="card-header">
                    <h3 class="card-title">Lịch Sử Chức Vụ Đảng</h3>
                </div>
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">      
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Chức Vụ Đảng</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngày Nhận Chức</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($party_position)): ?>
                                <?php $i = 1;?>
                                <?php foreach ($party_position as $personal_information): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$i++}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$personal_information->TenChucvuDang}}</p> 
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">
                                            {{date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Nhan_Chuc)))}}</p> 
                                        </td>

                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Lịch Sử Chức Vụ Đoàn</h3>
                </div>
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">      
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Chức Vụ Đoàn</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngày Nhận Chức</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($corporate_position)): ?>
                                <?php $i = 1;?>
                                <?php foreach ($corporate_position as $personal_information): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$i++}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$personal_information->TenChucvuDoan}}</p> 
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">
                                            {{date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Nhan_Chuc)))}}</p> 
                                        </td>

                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6"> 
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Lịch Sử Chức Vụ Chính Quyền</h3>
                </div>
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">      
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Chức Vụ Chính Quyền</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngày Nhận Chức</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($goverment_position)): ?>
                                <?php $i = 1;?>
                                <?php foreach ($goverment_position as $ds): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$i++}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$ds->TenChucvuCQ}}</p> 
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">
                                            {{date('d-m-Y',strtotime(str_replace('/','-',$ds->Ngay_Nhan_Chuc)))}}</p> 
                                        </td>

                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>

@stop