@extends('layout.admin')

@section('module_user')
<section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Sửa Thông Tin Nhân Viên</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Nhân Viên</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
 <div class="container-fluid">
    <div class="row">
          <!-- left column -->
      <div class="col-md-12">
            <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Sửa Thông Tin Nhân Viên</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="Ma_NV" value="{{$personal_information->Ma_NV}}">
            <input type="hidden" name="id" value="{{$personal_information->id}}">
            <div class="card-body">
              
                <div class="row">
                  <div class="col-12 col-md-6">
                    <label>Họ Tên Nhân Viên</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten" class="form-control" placeholder="Họ Tên Nhân Viên" value="{{$personal_information->Ho_Ten}}">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div>               
                </div>
                <div class="col-12 col-md-6">
                  <div class="row">
                      <label class="file"></label>
                      <input type="file" id="file-field" onchange="previewImage(event)" name="Avatar" aria-label="File browser example" style="margin: 35px 20px 20px 20px"><br>
                      <img src="/adminlte/dist/img/{{$personal_information->Avatar}}" id="image-field" width="125" height="125" style="margin: 0px 20px 20px 20px;border-radius: 20px">
                      <span id="file-field" style="color:red;text-align: right;"></span>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                  <label>Giới Tính</label>
                    <select  class="select form-control" name="Gioi_Tinh" id="select2">
                        <option value="" disabled selected>Hãy Giới Tính</option>
                        <option value="Nam">Nam</option>
                        <option value="Nữ">Nữ</option>
                    </select>
                </div>

                <div class="col-12 col-md-6">
                  <label>Ngày Sinh</label>
                  <div class="input-group mb-3" style="width: 80%;">
                      <div class="input-group date" id="reservationdate" data-target-input="nearest">
                          <input type="text" value="{{date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Sinh)))}}" name="Ngay_Sinh" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                          <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                      </div>
                  </div>
                </div>

              </div>
              <label>Nơi Ở Hiện Tại</label>
                <div class="input-group mb-3" style="align-content: center;">
                  <input type="text" name="Noi_O_Hien_Tai" value="{{$personal_information->Noi_O_Hien_Tai}}" class="form-control" placeholder="Nơi Ở Hiện Tại">
                  <div class="input-group-append">
                      <div class="input-group-text">
                          <span class="fas fa-users"></span>
                      </div>
                  </div>
               </div>  
               <div class="row">
                 <div class="col-12 col-md-4">
                    <label>Quê Quán</label>
                    <div class="input-group mb-3" style="align-content: center;">
                    <input type="text" name="Que_Quan" value="{{$personal_information->Que_Quan}}" class="form-control" placeholder="Quê quán">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                    </div> 
                 </div>

                 <div class="col-12 col-md-4">
                    <label>Tôn Giáo</label>
                    <div class="input-group mb-3" style="align-content: center;">
                    <input type="text" name="Ton_Giao" class="form-control" value="{{$personal_information->Ton_Giao}}" placeholder="Tôn Giáo">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                    </div> 
                 </div>

                 <div class="col-12 col-md-4">
                    <label>Dân Tộc</label>
                    <div class="input-group mb-3" style="align-content: center;">
                    <input type="text" name="Dan_Toc" class="form-control" value="{{$personal_information->Dan_Toc}}" placeholder="Dân Tộc">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                    </div> 
                 </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <label>Lý Luận Chính Trị</label>
                    <select  class="select form-control" name="Ly_Luan_Chinh_Tri" >
                        <option value="" disabled selected>Hãy Lý Luận Chính Trị</option>
                        <option value="Cao Cấp">Cao Cấp</option>
                        <option value="Trung Cấp">Trung Cấp</option>
                        <option value="Sơ Cấp">Sơ Cấp</option>
                        <option value="Tương Đương">Tương Đương</option>
                    </select>
                  </div>
                  <div class="col-12 col-md-6">
                    <label>Trình Độ</label>
                    <select  class="select form-control" onchange="ChangeCarList(this)" name="Trinh_Do" id="Trinh_Do">
                        <option value="" disabled selected>Hãy Chọn Trình Độ</option>
                        <?php if(isset($degrees)): ?>
                        <?php foreach ($degrees as $ds): ?>
                            <option value="{{$ds->id}}">{{$ds->Ten_trinh_do}}</option>
                        <?php endforeach; ?>     
                        <?php endif; ?> 
                    </select>
                  </div>
                </div>
               <div class="row">
                <div class="col-12 col-md-6">
                  <label>Ngoại Ngữ</label>
                  <div class="input-group mb-3" style="align-content: center;">
                  <input type="text" name="Ngoai_Ngu" value="{{$personal_information->Ngoai_Ngu}}" class="form-control" placeholder="Dân Tộc">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-users"></span>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <label>Tin Học</label>
                  <div class="input-group mb-3" style="align-content: center;">
                  <input type="text" name="Tin_Hoc"  class="form-control" value="{{$personal_information->Tin_Hoc}}" placeholder="Dân Tộc">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-users"></span>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
             <div class="row">
                 <div class="col-12 col-md-6">
                  <label>Ngày Vào Đảng</label>
                  <div class="input-group mb-3">
                      <div class="input-group date"  id="reservationdate1" data-target-input="nearest">
                          <input type="text" name="Ngay_Vao_Dang" value="{{ date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Vao_Dang)))}}" class="form-control datetimepicker-input" data-target="#reservationdate1"/>
                          <div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                      </div>
                  </div>
                </div>

                <div class="col-12 col-md-6">
                    <label>Ngày Vào Đoàn</label>
                  <div class="input-group mb-3">
                      <div class="input-group date"  id="reservationdate2" data-target-input="nearest">
                          <input type="text" name="Ngay_Vao_Doan" value="{{ date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Vao_Doan)))}}"
                           class="form-control datetimepicker-input" data-target="#reservationdate2"/>
                          <div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                      </div>
                  </div>
                </div>

              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                  <label>Ngày Vào Làm</label>
                  <div class="input-group mb-3">
                      <div class="input-group date"  id="reservationdate3" data-target-input="nearest">
                          <input type="text" name="Ngay_Vao_Lam" value="{{ date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Vao_Lam)))}}" class="form-control datetimepicker-input" data-target="#reservationdate3"/>
                          <div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                      </div>
                  </div>
                </div>

                <div class="col-12 col-md-6">
                  <label>Tình Trạng Làm Việc</label>
                    <select  class="select form-control" name="Tinh_Trang_Lam_Viec">
                        <option value="" disabled selected>Tình Trạng Làm Việc</option>
                        <option value="Làm Việc">Làm Việc</option>
                        <option value="Đã Nghỉ Việc">Đã Nghỉ Việc</option>
                    </select>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                  <label>Lương Cơ Bản</label>
                  <div class="input-group mb-3" style="align-content: center;">
                  <input type="text" name="Luong_CB" class="form-control" value="{{$personal_information->Luong_CB}}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-users"></span>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <label>Văn Bằng Khác</label>
                  <div class="input-group mb-3" style="align-content: center;">
                  <input type="text" name="Van_Bang" class="form-control" value="{{$personal_information->Van_Bang_Khac}}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-users"></span>
                        </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                 <div class="col-12 col-md-4">
                    <label>Chức Vụ Chính Quyền</label>
                    <select  class="select form-control" name="government_positions_id" >
                      <option value="" disabled selected>Hãy Chọn Chức Vụ Chính Quyền</option>
                      <?php if(isset($goverment_positions)): ?>
                      <?php foreach ($goverment_positions as $ds): ?>
                          <option value="{{$ds->id}}">{{$ds->TenChucvuCQ}}</option>
                      <?php endforeach; ?>     
                      <?php endif; ?> 
                    </select> 
                 </div>

                 <div class="col-12 col-md-4">
                    <label>chức Vụ Đoàn</label>
                    <select  class="select form-control" name="corporate_position_id" >
                      <option value="" disabled selected>Hãy Chọn Chức Vụ Đoàn</option>
                      <?php if(isset($corporate_positions)): ?>
                      <?php foreach ($corporate_positions as $ds): ?>
                          <option value="{{$ds->id}}">{{$ds->TenChucvuDoan}}</option>
                      <?php endforeach; ?>     
                      <?php endif; ?> 
                    </select> 
                 </div>

                 <div class="col-12 col-md-4">
                    <label>chức Vụ Đảng</label>
                    <select  class="select form-control" name="party_position_id" >
                      <option value="" disabled selected>Hãy Chọn Chức Vụ Đảng</option>
                      <?php if(isset($party_positions)): ?>
                      <?php foreach ($party_positions as $ds): ?>
                          <option value="{{$ds->id}}">{{$ds->TenChucvuDang}}</option>
                      <?php endforeach; ?>     
                      <?php endif; ?> 
                    </select> 
                 </div>
              </div>
           
              <div class="row">
                   <div class="col-12 col-md-4">
                      <label>Nghiệp Vụ</label>
                      <select  class="select form-control" name="major_id">
                        <option value="" disabled selected>Hãy Chọn Nghiệp Vụ</option>
                        <?php if(isset($majors)): ?>
                        <?php foreach ($majors as $ds): ?>
                            <option value="{{$ds->id}}">{{$ds->Ten_Nghiep_Vu}}</option>
                        <?php endforeach; ?>     
                        <?php endif; ?> 
                      </select> 
                   </div>

                   <div class="col-12 col-md-4">
                      <label>Cấp Bậc</label>
                      <select  class="select form-control" name="rank_id" id="rank_id">
                        <option value="" disabled selected>Hãy Chọn Cấp Bậc </option>
                      </select> 
                   </div>

                   <div class="col-12 col-md-4">
                     <label>Phòng Ban</label>
                     <select class="select form-control" name="room_id">
                       <option value="" disabled selected>Hãy Chọn Phòng Ban</option>
                        <?php if(isset($rooms)): ?>
                        <?php foreach ($rooms as $ds): ?>
                            <option value="{{$ds->id}}">{{$ds->Ten_PB}}</option>
                        <?php endforeach; ?>     
                        <?php endif; ?> 
                     </select>
                   </div>
              </div>
            </div>
            <!-- /.card-body -->
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
 
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
  $(function () {

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $('#reservationdate1').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#reservationdate2').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#reservationdate3').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script type="text/javascript">
  function previewImage(){
    var reader = new FileReader();
    var imageField = document.getElementById("image-field");

    reader.onload = function(){
      if(reader.readyState == 2){
        imageField.src = reader.result;
      }
    }
    reader.readAsDataURL(event.target.files[0]);
  }
</script>

<script>
function ChangeCarList(obj) {
  var carList = document.getElementById("Trinh_Do");
  var modelList = document.getElementById("rank_id");
  var bien = obj.value;
  var bien1 = "";
  var ten = "";
  var i;
  while (modelList.options.length) {
    modelList.remove(0);
  }
  if (bien == 1) 
  {
    for (i = 1; i <= 12; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }
  else if(bien == 2)
  {
    for (i = 1; i <= 15; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }
  else if(bien == 3){
    for (i = 1; i <= 19; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }
  else if(bien == 4){
    for (i = 1; i <= 27; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }

  else if(bien == 5)
  {
    for (i = 1; i <= 35; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }

  else
  {
    for (i = 1; i <= 43; i++) 
    {
      <?php foreach ($ranks as $dvm): ?>
        if(i === {{$dvm->id}})
        {
          bien1 = "{{$dvm->id}}";
          ten = "{{$dvm->Ten_Cap_Bac}}";
        }
      <?php endforeach ?>
      var car = new Option(ten, i);
      modelList.options.add(car);
      console.log(bien1);
    }
  }
} 
</script>
<script type="text/javascript">
  window.onload = function test()
  {
    var bien_dem;
    bien_dem = "{{$r}}";
    if(bien_dem > 1)
    {
      $("#test").hide();
      $("#Van_Bang").addClass("col-md-12")
    }    
    else
    {
      console.log(2);
    }
  }
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
@endsection
 