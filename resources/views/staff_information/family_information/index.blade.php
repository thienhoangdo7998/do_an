@extends('layout.admin')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Hoàn Cảnh Gia Đình</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Hoàn Cảnh Gia Đình</li>
         </ol>
        </div>
      </div>
    </div>
</section>


<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Danh Sách Phòng Ban</h3>
                </div>
            <!-- /.card-header -->
                <div class=" card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Họ Tên Nhân Viên</th>
                                    <th style="text-align: center; vertical-align: middle;">Họ Tên Cha</th>
                                    <th style="text-align: center; vertical-align: middle;">Nghề NGhiệp Cha</th>
                                    <th style="text-align: center; vertical-align: middle;">Nơi Sinh Cha</th>
                                    <th style="text-align: center; vertical-align: middle;">Họ Tên Mẹ</th>
                                    <th style="text-align: center; vertical-align: middle;">Nghề NGhiệp Mẹ</th>
                                    <th style="text-align: center; vertical-align: middle;">Nơi Sinh Mẹ</th>
                                    <th style="text-align: center; vertical-align: middle;">Họ Tên Vợ\Chồng</th>
                                    <th style="text-align: center; vertical-align: middle;">Nghề NGhiệp Vợ\Chồng</th>
                                    <th style="text-align: center; vertical-align: middle;">Nơi Sinh Vợ\Chồng</th>
                                    <th style="text-align: center; vertical-align: middle;">Họ Tên Con 1</th>
                                    <th style="text-align: center; vertical-align: middle;">Nghề NGhiệp Con 1</th>
                                    <th style="text-align: center; vertical-align: middle;">Họ Tên Con 2</th>
                                    <th style="text-align: center; vertical-align: middle;">Nghề NGhiệp Con 2</th>
                                    <th style="text-align: center; vertical-align: middle;">Họ Tên Con 3</th>
                                    <th style="text-align: center; vertical-align: middle;">Nghề NGhiệp Con 3</th>
                                    <th style="text-align: center; vertical-align: middle;">Xóa</th>
                                    <th style="text-align: center; vertical-align: middle;">Sửa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($family_informations)): ?>
                                    <?php $i = 1;?>
                                <?php foreach ($family_informations as $family_information): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$i++}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Ho_Ten}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Ho_Ten_Cha}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Nghe_Nghiep_Cha}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Noi_Sinh_Cha}}</a>
                                        </td>
                                        
                                         <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Ho_Ten_Me}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Nghe_Nghiep_Me}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Noi_Sinh_Me}}</a>
                                        </td>


                                         <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Ho_Ten_Vo_Chong}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Nghe_Nghiep_Vo_Chong}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Noi_Sinh_Vo_Chong}}</a>
                                        </td>

                                         <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Ho_Ten_Con1}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Nghe_Nghiep_Con1}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Ho_Ten_Con2}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Nghe_Nghiep_Con2}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Ho_Ten_Con3}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$family_information->Nghe_Nghiep_Con3}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{route('family_information.delete',[$family_information->id])}}"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-block" href="{{route('family_information.edit',[$family_information->id])}}" > <i class="fas fa-pen"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
           </div>
        </div>
    </div>
</section>  


@stop

