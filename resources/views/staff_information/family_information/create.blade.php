
 <div class="container-fluid">
    <div class="row">
          <!-- left column -->
      <div class="col-md-12">
            <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Hoàn Cảnh Gia Đình</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->

            <div class="card-body">
              <div class="form-group">
                <div class="row">
                    <label style="margin: 0px 10px 0px 9px">Thông Tin Cha:</label>
                    <div class="form-group mb-0">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="terms" id="thongtincha" class="custom-control-input" >
                        <label class="custom-control-label" for="thongtincha">Đã mất</label>
                      </div>
                    </div>
                </div>

               <div class="row">
                 <div class="col-12 col-md-4">
                   <label>Họ Tên Cha</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Cha" id="Ho_Ten_Cha" class="form-control" placeholder="Họ Tên Cha">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nghề Nghiệp Cha</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Cha" id="Nghe_Nghiep_Cha" class="form-control" placeholder="Nghề Nghiệp Cha">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nơi Sinh Cha</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Noi_Sinh_Cha" id="Noi_Sinh_Cha" class="form-control" placeholder="Nơi Sinh Cha">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

               <div class="row">
                  <label style="margin: 0px 10px 0px 9px">Thông Tin Mẹ:</label>
                  <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" id="thongtinme" class="custom-control-input" >
                      <label class="custom-control-label" for="thongtinme">Đã mất</label>
                    </div>
                  </div>
                </div>

               <div class="row">
                 <div class="col-12 col-md-4">
                   <label>Họ Tên Mẹ</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Me" id="Ho_Ten_Me" class="form-control" placeholder="Họ Tên Mẹ">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nghề Nghiệp Mẹ</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Me" id="Nghe_Nghiep_Me" class="form-control" placeholder="Nghề Nghiệp Mẹ">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nơi Sinh Mẹ</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Noi_Sinh_Me" id="Noi_Sinh_Me" class="form-control" placeholder="Nơi Sinh Mẹ">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

               <div class="row">
                    <label style="margin: 0px 10px 0px 9px">Thông Tin Vợ-Chồng</label>
                    <div class="form-group mb-0">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="terms" id="thongtinchong/vo" class="custom-control-input" >
                        <label class="custom-control-label" for="thongtinchong/vo">Chưa kết hôn</label>
                      </div>
                    </div>
                </div>

               <div class="row">
                 <div class="col-12 col-md-4">
                   <label>Họ Tên Vợ-Chồng:</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Vo/Chong" id="Ho_Ten_Vo/Chong" class="form-control" placeholder="Họ Tên Vợ Chồng">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nghề Nghiệp Vợ-Chồng</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Vo/Chong" id="Nghe_Nghiep_Vo/Chong" class="form-control" placeholder="Nghề Nghiệp Vợ Chồng">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nơi Sinh Vợ-Chồng</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Noi_Sinh_Vo/Chong" id="Noi_Sinh_Vo/Chong" class="form-control" placeholder="Nơi Sinh Vợ Chồng">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

               <div class="row">
                    <label style="margin: 0px 10px 0px 9px">Thông Tin Con:</label>
                    <div class="form-group mb-0">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="terms" id="thongtincon" class="custom-control-input" >
                        <label class="custom-control-label" for="thongtincon">Không có</label>
                      </div>
                    </div>
                </div>

               <div class="row">
                 <div class="col-12 col-md-6">
                   <label>Họ Tên Con 1</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Con1" id="Ho_Ten_Con1" class="form-control" placeholder="Họ Tên Con 1">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-6">
                   <label>Nghề Nghiệp Con 1</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Con1" id="Nghe_Nghiep_Con1" class="form-control" placeholder="Nghề Nghệp Con 1">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

                <div class="row">
                 <div class="col-12 col-md-6">
                   <label>Họ Tên Con 1</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Con2" id="Ho_Ten_Con2" class="form-control" placeholder="Họ Tên Con 2">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-6">
                   <label>Nghề Nghiệp Con 2</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Con2" id="Nghe_Nghiep_Con2" class="form-control" placeholder="Nghề Nghiệp Con 2">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

                <div class="row">
                 <div class="col-12 col-md-6">
                   <label>Họ Tên Con 3</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Con3" id="Ho_Ten_Con3" class="form-control" placeholder="Họ Tên Con 3">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-6">
                   <label>Nghề Nghiệp Con 3</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Con3" id="Nghe_Nghiep_Con3" class="form-control" placeholder="Nghề Nghiệp Con 3">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 

                 </div>   

               </div>
              </div>
            <!-- /.card-body -->
            </div>
             <div class="card-footer">
                <button type="submit" class="btn btn-primary" value="upload">Thêm Thông Tin Nhân Viên</button>
              </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript">

  document.getElementById("thongtincha").onclick = function () 
  {
    if(this.checked)
    {
      document.getElementById("Ho_Ten_Cha").disabled = 'true';
      document.getElementById("Nghe_Nghiep_Cha").disabled = 'true';
      document.getElementById("Noi_Sinh_Cha").disabled = 'true';
    }
    else
    {
      document.getElementById("Ho_Ten_Cha").removeAttribute("disabled");
      document.getElementById("Nghe_Nghiep_Cha").removeAttribute("disabled");
      document.getElementById("Noi_Sinh_Cha").removeAttribute("disabled");
    }
  };
  document.getElementById("thongtinme").onclick = function () 
  {
    if(this.checked)
    {
      document.getElementById("Ho_Ten_Me").disabled = 'true';
      document.getElementById("Nghe_Nghiep_Me").disabled = 'true';
      document.getElementById("Noi_Sinh_Me").disabled = 'true';
    }
    else
    {
      document.getElementById("Ho_Ten_Me").removeAttribute("disabled");
      document.getElementById("Nghe_Nghiep_Me").removeAttribute("disabled");
      document.getElementById("Noi_Sinh_Me").removeAttribute("disabled");
    }
  };
  document.getElementById("thongtinchong/vo").onclick = function () 
  {
    if(this.checked)
    {
      document.getElementById("Ho_Ten_Vo/Chong").disabled = 'true';
      document.getElementById("Nghe_Nghiep_Vo/Chong").disabled = 'true';
      document.getElementById("Noi_Sinh_Vo/Chong").disabled = 'true';
    }
    else
    {
      document.getElementById("Ho_Ten_Vo/Chong").removeAttribute("disabled");
      document.getElementById("Nghe_Nghiep_Vo/Chong").removeAttribute("disabled");
      document.getElementById("Noi_Sinh_Vo/Chong").removeAttribute("disabled");
    }
  };

  document.getElementById("thongtincon").onclick = function () 
  {
    if(this.checked)
    {
      document.getElementById("Ho_Ten_Con1").disabled = 'true';
      document.getElementById("Nghe_Nghiep_Con1").disabled = 'true';
      document.getElementById("Ho_Ten_Con2").disabled = 'true';
      document.getElementById("Nghe_Nghiep_Con2").disabled = 'true';
      document.getElementById("Ho_Ten_Con3").disabled = 'true';
      document.getElementById("Nghe_Nghiep_Con3").disabled = 'true';
    }
    else
    {
      document.getElementById("Ho_Ten_Con1").removeAttribute("disabled");
      document.getElementById("Nghe_Nghiep_Con1").removeAttribute("disabled");
      document.getElementById("Ho_Ten_Con2").removeAttribute("disabled");
      document.getElementById("Nghe_Nghiep_Con2").removeAttribute("disabled");
      document.getElementById("Ho_Ten_Con3").removeAttribute("disabled");
      document.getElementById("Nghe_Nghiep_Con3").removeAttribute("disabled");
    }
  };
</script>


 