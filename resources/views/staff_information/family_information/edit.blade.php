@extends('layout.admin')

@section('module_user')
<section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thêm Thông Tin Nhân Viên</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Nhân Viên</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
 <div class="container-fluid">
    <div class="row">
          <!-- left column -->
      <div class="col-md-12">
            <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thêm Thông Tin Nhân Viên</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" method="post" action="">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="id" value="{{$family_information->id}}">
            <input type="hidden" name="staff_id" value="{{$family_information->id_staff}}">
            <div class="card-body">
              <div class="form-group">
                <div class="row">
                    <label>Thông Tin Cha</label>
                    <div class="form-group mb-0">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="terms"  id="thongtincha" class="custom-control-input" >
                        <label class="custom-control-label" for="thongtincha">Đã mất</label>
                      </div>
                    </div>
                </div>

               <div class="row">
                 <div class="col-12 col-md-4">
                   <label>Họ Tên Cha</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Cha" value="{{$family_information->Ho_Ten_Cha}}" id="Ho_Ten_Cha" class="form-control" placeholder="Họ Tên Cha">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nghề Nghiệp Cha</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Cha" id="Nghe_Nghiep_Cha" value="{{$family_information->Nghe_Nghiep_Cha}}" class="form-control" placeholder="Họ Tên Cha">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nơi Sinh Cha</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Noi_Sinh_Cha" id="Noi_Sinh_Cha"value="{{$family_information->Noi_Sinh_Cha}}" class="form-control" placeholder="Họ Tên Cha">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

               <div class="row">
                 <div class="col-12 col-md-4">
                   <label>Họ Tên Mẹ</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Me" value="{{$family_information->Ho_Ten_Me}}" class="form-control" placeholder="Họ Tên Mẹ">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nghề Nghiệp Mẹ</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Me" id="Nghe_Nghiep_Me" value="{{$family_information->Nghe_Nghiep_Me}}" class="form-control" placeholder="Nghề Nghiệp Mẹ">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nơi Sinh Mẹ</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Noi_Sinh_Me" id="Noi_Sinh_Me" value="{{$family_information->Noi_Sinh_Me}}" class="form-control" placeholder="Nơi Sinh Mẹ">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

               <div class="row">
                 <div class="col-12 col-md-4">
                   <label>Họ Tên Vợ\Chồng</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Vo/Chong" id="Ho_Ten_Vo/Chong" value="{{$family_information->Ho_Ten_Vo_Chong}}" class="form-control" placeholder="Họ Tên Vợ\Chồng">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nghề Nghiệp Vợ\Chồng</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Vo/Chong" id="Nghe_Nghiep_Vo/Chong" value="{{$family_information->Nghe_Nghiep_Vo_Chong}}" class="form-control" placeholder="Nghề Nghiệp Vợ Chồng">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-4">
                   <label>Nơi Sinh Vợ/Chồng</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Noi_Sinh_Vo/Chong" value="{{$family_information->Noi_Sinh_Vo_Chong}}" id="Noi_Sinh_Vo/Chong" class="form-control" placeholder="Nơi Sinh Vợ\Chồng">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

               <div class="row">
                 <div class="col-12 col-md-6">
                   <label>Họ Tên Con 1</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Con1" id="Ho_Ten_Con1" value="{{$family_information->Ho_Ten_Con1}}" class="form-control" placeholder="Họ Tên Con 1">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-6">
                   <label>Nghề Nghiệp Con 1</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Con1" id="Nghe_Nghiep_Con1" value="{{$family_information->Nghe_Nghiep_Con1}}" class="form-control" placeholder="Nghề Nghệp Con 1">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

                <div class="row">
                 <div class="col-12 col-md-6">
                   <label>Họ Tên Con 1</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Con2" id="Ho_Ten_Con2" value="{{$family_information->Ho_Ten_Con2}}" class="form-control" placeholder="Họ Tên Con 2">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-6">
                   <label>Nghề Nghiệp Con 2</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Con2" id="Nghe_Nghiep_Con2" value="{{$family_information->Nghe_Nghiep_Con2}}" class="form-control" placeholder="Nghề Nghiệp Con 2">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>               
               </div>

                <div class="row">
                 <div class="col-12 col-md-6">
                   <label>Họ Tên Con 3</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Ho_Ten_Con3" id="Ho_Ten_Con3" class="form-control" value="{{$family_information->Ho_Ten_Con3}}" placeholder="Họ Tên Con 3">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 
                 </div>

                 <div class="col-12 col-md-6">
                   <label>Nghề Nghiệp Con 3</label>
                    <div class="input-group mb-3" style="align-content: center;">
                      <input type="text" name="Nghe_Nghiep_Con3" value="{{$family_information->Nghe_Nghiep_Con3}}"id="Nghe_Nghiep_Con3" class="form-control" placeholder="Nghề Nghiệp Con 3">
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-users"></span>
                          </div>
                      </div>
                   </div> 

                 </div>   

               </div>
              </div>
            <!-- /.card-body -->
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<script language="javascript">

  document.getElementById("thongtincha").onclick = function () 
  {
    if(this.checked)
    {
      document.getElementById("Ho_Ten_Cha").disabled = 'true';
      document.getElementById("Nghe_Nghiep_Cha").disabled = 'true';
      document.getElementById("Noi_Sinh_Cha").disabled = 'true';
    }
    else
    {
      document.getElementById("Ho_Ten_Cha").removeAttribute("disabled");
      document.getElementById("Nghe_Nghiep_Cha").removeAttribute("disabled");
      document.getElementById("Noi_Sinh_Cha").removeAttribute("disabled");
    }
  };

</script>

@endsection
 