@extends('layout.admin')

@section('module_user')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Trang Chủ</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Trang Chủ</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h3>{{count($hsl)}}</h3>

                    <p>Tăng Hệ Số Lương</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-cash"></i>
                  </div>
                  <a href="{{route('staff.hsl')}}" class="small-box-footer">Nâng Hệ Số Lương <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h3>{{count($lenham)}}<sup style="font-size: 20px"></sup></h3>

                    <p>Nhân Viên Cần Lên Hàm</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-star"></i>
                  </div>
                  <a href="{{route('staff.lenham1')}}" class="small-box-footer">Nhân Viên Lên Hàm <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                  <div class="inner">
                    <h3>{{count($personal_informations)}}</h3>

                    <p>Số Lượng Nhân Viên</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person-add"></i>
                  </div>
                  <a href="{{route('staff.index')}}" class="small-box-footer">Thông Tin Nhân Viên <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                  <div class="inner">
                    <h3>{{count($staff_bonus_disciplines)}}</h3>

                    <p>Xác Nhận Khen Thưởng-Kỉ Luật</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-clipboard"></i>
                  </div>
                  <a href="{{route('staff_bonus_discipline.show_level_up')}}" class="small-box-footer">Khen Thưởng Kỉ Luật <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
        </div>
      </div> 
    </section>
@stop