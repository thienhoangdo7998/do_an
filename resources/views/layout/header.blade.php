  <aside class="main-sidebar sidebar-dark-primary elevation-4" >
    <!-- Brand Logo -->
    <a href="{{route('home_page')}}" class="brand-link">
      <img src="{{asset('adminlte/dist/img/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Quản Lý CBCS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-tie"></i>
              <p>
                Thông Tin Nhân Viên
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('staff.index')}}" class="nav-link">
                  <i class="far fa-id-card nav-icon"></i>
                  <p style="font-size: 13px"> Thông Tin Cá Nhân</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{route('family_information.index')}}" class="nav-link">
                  <i class="fas fa-home nav-icon"></i>
                  <p style="font-size: 13px">Quan Hệ Gia Đình</p>
                </a>
              </li>
            </ul>
          </li>
           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-award"></i>
              <p>
                 Khen Thưởng Kỉ Luật
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('staff_bonus_discipline.index')}}" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p style="font-size: 13px">Danh Sách Khen Thưởng Kỉ Luật</p>
                </a>
              </li>
            </ul>
             <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('staff_bonus_discipline.show_level_up')}}" class="nav-link">
                  <i class="fas fa-clipboard-check nav-icon"></i>
                  <p style="font-size: 13px">Xác Nhận Khen Thưởng Kỉ Luật</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-angle-double-up"></i>
              <p>
                Thông Tin Cấp Bậc
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('rank.index')}}" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p style="font-size: 13px">Danh Sách Cấp Bậc</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('staff.hsl')}}" class="nav-link">
                  <i class="fas fa-money-bill-wave nav-icon"></i>
                  <span class="badge badge-info right"></span>
                  <p style="font-size: 13px">Nâng Hệ Số Lương</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('staff.lenham1')}}" class="nav-link">
                  <i class="far fa-star nav-icon"></i>
                  <p style="font-size: 13px">Nhân Viên Lên Hàm</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('staff.history_tangham')}}" class="nav-link">
                  <i class="fas fa-book nav-icon"></i>
                  <p style="font-size: 13px">Lịch Sử Nhân Viên</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-fist-raised"></i>
              <p>
                Chức Vụ Đoàn
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('corporate_position.index')}}" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p style="font-size: 13px">Danh Sách Chức Vụ Đoàn</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('corporate_position.update_corporate')}}" class="nav-link">
                  <i class="fas fa-arrow-alt-circle-up nav-icon"></i>
                  <p style="font-size: 13px">Thay Đổi Chức Vụ Đoàn</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-gavel"></i>
              <p>
                Chức Vụ Đảng
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('party_position.index')}}" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p style="font-size: 13px">Danh Sách Chức Vụ Đảng</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('corporate_position.update_corporate')}}" class="nav-link">
                  <i class="fas fa-arrow-alt-circle-up nav-icon"></i>
                  <p style="font-size: 13px">Thay Đổi Chức Vụ Đảng</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-university"></i>
              <p>
                Chức Vụ Chính Quyền
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('goverment_position.index')}}" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p style="font-size: 13px">Danh Sách Chức Vụ Chính Quyền</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('goverment_position.update_goverment')}}" class="nav-link">
                  <i class="fas fa-arrow-alt-circle-up nav-icon"></i>
                  <p style="font-size: 13px">Thay Đổi Chức Vụ Chính Quyền</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{route('room_index')}}" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p><font size="3">
                Thông Tin Phòng Ban
              </font>
              </p>
            </a>
          </li>
       
          <li class="nav-item">
            <a href="{{route('major.index')}}" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p><font size="3">
                Thông Tin Nghiệp Vụ
              </font>
              </p>
            </a>
          </li>

          <li class="nav-header" style="font-size: +5">Tài Khoản</li>
           <li class="nav-item">
            <a href="{{route('user')}}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p><font size="3">
                Thông Tin Tài Khoản
              </font>
              </p>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{route('Role.index')}}" class="nav-link">
              <i class="nav-icon fas fa-users-cog"></i>
              <p><font size="3">
                Phần Quyền Người Dùng
              </font>
              </p>
            </a>
          </li>
        </ul>
      </nav>


      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>