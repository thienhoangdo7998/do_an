@extends('layout.admin')

@section('content')
 <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Danh Sách {{$listnv[0]->TenChucvuCQ}}</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">{{$listnv[0]->TenChucvuCQ}}</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Danh Sách Chức Vụ {{$listnv[0]->TenChucvuCQ}}</h3>
                </div>
            <!-- /.card-header -->
                <div class=" card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Mã Nhân Viên</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Chức Vụ Chính Quyền</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($listnv)): ?>
                                <?php $i = 1;?>
                                <?php foreach ($listnv as $government_position): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p>{{$i++}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p>{{$government_position->Ma_NV}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p>{{$government_position->Ho_Ten}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p>{{$government_position->TenChucvuCQ}}</p>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
@stop

