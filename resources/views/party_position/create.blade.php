

@extends('layout.admin')

@section('module_user')
      <div class="content-header">

        <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>thêm Chức Vụ Đảng</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Chức Vụ Đảng</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
          <section class="content">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-12">

                        <div class="card">
                          <div class="card-header">
                            <h3 class="card-title">Thêm Thông Tin chức Vụ Đảng</h3>
                          </div>

                              <form role="form"  method="post" action="">
                                <div class="card-body">
                                  <div class="form-group">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <label>Tên Chức Vụ Đảng</label>
                                    <div class="input-group mb-3" style="align-content: center;">
                                    <input type="text" name="tencvdang" class="form-control" placeholder="Tên Chức Vụ Đoàn Thể">
                                    <div class="input-group-append">
                                      <div class="input-group-text">
                                          <span class="fas fa-users"></span>
                                      </div>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="card-footer" >
                                    <button type="submit" class="btn btn-primary">Thêm Người Dùng</button>
                                </div>
                               
                              </form>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
          </section>
      </div>
  </div>


                
                


@endsection