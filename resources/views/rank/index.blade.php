@extends('layout.admin')

@section('content')
 <div class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thông Tin Cấp Bậc</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Cấp Bậc</li>
                 </ol>
                </div>
              </div>
            </div>
          </div>
<section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Danh Sách Cấp Bậc</h3>
            </div>
            <!-- /.card-header -->
            <div class=" card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" >
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle;">Id</th>
                                <th style="text-align: center; vertical-align: middle;">Tên</th>
                                <th style="text-align: center; vertical-align: middle;">Hệ Số Lương</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($ranks)): ?>
                            <?php $i = 1;?>
                            <?php foreach ($ranks as $rank): ?>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$i++}}</a>
                                    </td>

                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$rank->Ten_Cap_Bac}}</a>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$rank->He_So_Luong}}</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php endif; ?> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</section>
@stop


