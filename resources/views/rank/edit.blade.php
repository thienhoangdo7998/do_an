@extends('layout.admin')

@section('module_user')

          <div class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Sửa Cấp Bậc</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Cấp Bậc</li>
                 </ol>
                </div>
              </div>
            </div>
          </div>

          <section class="content">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-12">

                        <div class="card">
                          <div class="card-header">
                            <h3 class="card-title">Sửa Thông Tin Cấp Bậc</h3>
                          </div>

                              <form role="form"  method="post" action="">
                                <div class="card-body">
                                  <div class="form-group">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="id" value="{{$rank->id}}">
                                    <label>Tên Cấp Bậc</label>
                                    <div class="input-group mb-3" style="align-content: center;">
                                    <input type="text" name="ten_cap_bac" class="form-control" placeholder="Tên Cấp Bậc" value="{{$rank->Ten_Cap_Bac}}">
                                    <div class="input-group-append">
                                      <div class="input-group-text">
                                          <span class="fas fa-users"></span>
                                      </div>
                                    </div>
                                   </div>
                                   <label>Hệ Số Lương</label>
                                    <div class="input-group mb-3" style="align-content: center;">
                                    <input type="number" class="form-control" placeholder="Hệ Số Lương" required name="he_so_luong" value="{{$rank->He_So_Luong}}" min="0"  step="0.1" title="Currency" pattern="^\d+(?:\.\d{1,2})?$" onblur="this.parentNode.parentNode.style.backgroundColor=/^\d+(?:\.\d{1,2})?$/.test(this.value)?'inherit':'red'">
                                    <div class="input-group-append">
                                      <div class="input-group-text">
                                          <span class="fas fa-users"></span>
                                      </div>
                                    </div>
                                  </div>
                              	</div>
                                </div>
                                <div class="card-footer" >
                                    <button type="submit" class="btn btn-primary">Sửa Cấp Bậc </button>
                                </div>
                               
                              </form>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
          </section>
      </div>
  </div>
</div>
@endsection
