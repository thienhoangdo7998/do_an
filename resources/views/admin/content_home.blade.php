@extends('admin.home')

@section('content')
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Hồ Sơ Cá Nhân</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="text-right">
                      <img class="rounded mx-auto d-block"
                           src="../../../adminlte/dist/img/{{$personal_informations[0]->Avatar}}"
                           alt="Cinque Terre" style="width: 200px; height: 200px">
                    </div>
                    <h3 class="profile-username text-center">{{$personal_informations[0]->Ho_Ten}}</h3>
                    <p class="text-muted text-center"></p>
                  </div>

                  <div class="col-sm-9">
                     <div class="row">
                      <div class="col-sm-6" style="margin: 0px 0px 0px 0px">
                        <ul class="list-group list-group-unbordered ">
                          <li class="list-group-item">
                            <label>Mã Nhân Viên: </label> <a>{{$personal_informations[0]->Ma_NV}}</a>
                          </li>
                          <li class="list-group-item">
                            <label>Trạng Thái: </label> <a>{{$personal_informations[0]->Tinh_Trang_Lam_Viec}}</a>
                          </li>
                          <li class="list-group-item">
                            <label>Cấp Bậc: </label> <a>{{$personal_informations[0]->Ten_Cap_Bac}}</a>
                          </li>
                          <li class="list-group-item">
                            <label>Chức Vụ Đoàn: </label> <a>{{$personal_informations[0]->TenChucvuDoan}}</a>
                          </li>
                          <li class="list-group-item">
                            <label>Chức Vụ Đảng: </label> <a>{{$personal_informations[0]->TenChucvuDang}}</a>
                          </li>
                          <li class="list-group-item">
                            <label>Chức Vụ Chính Quyền: </label> <a>{{$personal_informations[0]->TenChucvuCQ}}</a>
                          </li>
                        </ul>
                      </div>
                      <div class="col-sm-6">
                         <ul class="list-group list-group-unbordered ">
                          <li class="list-group-item">
                            <label>Giới Tính: </label> <a>{{$personal_informations[0]->Gioi_Tinh}}</a>
                          </li>
                          <li class="list-group-item">
                            <label>Phòng Ban: </label> <a>Đội 1</a>
                          </li>
                          <li class="list-group-item">
                            <label>Ngày Vào Làm: </label> <a>{{$personal_informations[0]->Ngay_Vao_Lam}}</a>
                          </li>
                          <li class="list-group-item">
                            <label>Ngày Vào Đoàn: </label> <a>{{$personal_informations[0]->Ngay_Vao_Doan}}</a>
                          </li>
                          <li class="list-group-item">
                            <label>Ngày Vào Đảng: </label> <a>{{$personal_informations[0]->Ngay_Vao_Dang}}</a>
                          </li>
                          <li class="list-group-item">
                            <label>Lý Luận Chính Trị: </label> <a>{{$personal_informations[0]->Ly_Luan_Chinh_Tri}}</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
          
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Thông Tin Cá Nhân</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Quan Hệ Gia Đình</a></li>

                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                      <div class="col-sm-12">
                       <div class="row">
                        <div class="col-sm-6" style="margin: 0px 0px 0px 0px">
                          <ul class="list-group list-group-unbordered ">
                            <li class="list-group-item">
                              <label>Ngày Sinh: </label> <a>{{$personal_informations[0]->Ngay_Sinh}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nơi Ở Hiện Tại: </label> <a>{{$personal_informations[0]->Noi_O_Hien_Tai}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Trình Độ: </label> <a>{{$personal_informations[0]->Ten_trinh_do}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Ngoại Ngữ: </label> <a>{{$personal_informations[0]->Ngoai_Ngu}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Tin Học: </label> <a>{{$personal_informations[0]->Tin_Hoc}}</a>
                            </li>
                            
                          </ul>
                        </div>
                        <div class="col-sm-6">
                           <ul class="list-group list-group-unbordered ">
                            <li class="list-group-item">
                              <label>Quê Quán: </label> <a>{{$personal_informations[0]->Que_Quan}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Email: </label> <a>{{$bien[0]->email}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Số Điện Thoại: </label> <a>1,322</a>
                            </li>
                            <li class="list-group-item">
                              <label>Dân Tộc: </label> <a>{{$personal_informations[0]->Dan_Toc}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Tôn Giáo: </label> <a>{{$personal_informations[0]->Ton_Giao}}</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-6">
                          <ul class="list-group list-group-unbordered ">
                            <li class="list-group-item">
                              <label>Họ Tên cha: </label> <a>{{$family_informations[0]->Ho_Ten_Cha}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nghề Nghiệp Cha: </label> <a>{{$family_informations[0]->Nghe_Nghiep_Cha}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nơi Sinh Cha: </label> <a>{{$family_informations[0]->Noi_Sinh_Cha}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Họ Tên Mẹ: </label> <a>{{$family_informations[0]->Noi_Sinh_Me}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nghề Nghiệp Mẹ: </label> <a>{{$family_informations[0]->Nghe_Nghiep_Me}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nơi Sinh Mẹ: </label> <a>{{$family_informations[0]->Noi_Sinh_Me}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Họ Tên Chồng\Vọ: </label> <a>{{$family_informations[0]->Ho_Ten_Vo_Chong}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nghề Nghiệp Chồng\Vợ: </label> <a>{{$family_informations[0]->Nghe_Nghiep_Vo_Chong}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nơi Sinh Chồng\Vợ: </label> <a>{{$family_informations[0]->Noi_Sinh_Vo_Chong}}</a>
                            </li>
                          </ul>
                        </div>
                        <div class="col-sm-6">
                          <ul class="list-group list-group-unbordered ">
                            <li class="list-group-item">
                              <label>Họ Tên Con 1: </label> <a>{{$family_informations[0]->Ho_Ten_Con1}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nghề Nghiệp Con 1: </label> <a>{{$family_informations[0]->Nghe_Nghiep_Con1}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Họ Tên Con 2: </label> <a>{{$family_informations[0]->Ho_Ten_Con2}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nghề Nghiệp Con 2: </label> <a>{{$family_informations[0]->Nghe_Nghiep_Con2}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Họ Tên Con 3: </label> <a>{{$family_informations[0]->Ho_Ten_Con3}}</a>
                            </li>
                            <li class="list-group-item">
                              <label>Nghề Nghiệp Con 3: </label> <a>{{$family_informations[0]->Nghe_Nghiep_Con3}}</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal">
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputName" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName2" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Experience</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection