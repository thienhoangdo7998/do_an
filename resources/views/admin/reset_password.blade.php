@extends('admin.home')

@section('content')
	<section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Thay Đổi Mật Khẩu</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item active"></li>
             </ol>
            </div>
          </div>
        </div>
     </section>
    <section class="content">
      <div class="container-fluid" style="size: 80%">
        <div class="row">
          <div class="col-md-6 col-md-offset-3" style="margin-left: 260px;margin-right: 260px">
             <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mật Khẩu Hiện Tại</label>
                    <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Mật Khẩu Hiện Tại">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mật Khẩu Mới</label>
                    <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Mật Khẩu Mới">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Nhập Lại Mật Khẩu Mới</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Nhập Lại Mật Khẩu Mới">
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Đổi Mật Khẩu</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  	</section>
@stop