@extends('layout.admin')

@section('content')
<section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thông Tin Khen Thưởng - Kỉ Luật</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Khen Thưởng - Kỉ Luật</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
<div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
    <a href="{{route('staff_bonus_discipline.create')}}" class="btn btn-primary btn-flat" class="btn btn-primary btn-flat" style="margin: 0px 0px 0px 10px;border-radius: 10px">
        <i class="fas fa-plus"></i> {{ trans('Thêm Khen Thưởng - Kỉ Luật') }}
    </a>
</div>

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Danh Sách Khen Thưởng Kỉ Luật</h3>
                </div>
            <!-- /.card-header -->
                <div class=" card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Hình Thức</th>
                                    <th style="text-align: center; vertical-align: middle;">Số Quyết Định</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                    <th style="text-align: center; vertical-align: middle;">Lý Do</th>
                                    <th style="text-align: center; vertical-align: middle;">Xóa</th>
                                    <th style="text-align: center; vertical-align: middle;">Sửa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($staff_bonus_disciplines)): ?>
                                    <?php $i = 1;?>
                                <?php foreach ($staff_bonus_disciplines as $staff_bonus_discipline): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$i++}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$staff_bonus_discipline->Hinh_Thuc}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{date('d-m-Y',strtotime(str_replace('/','-',$staff_bonus_discipline->Ngay_QD)))}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$staff_bonus_discipline->Ho_Ten}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$staff_bonus_discipline->Ly_Do}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{route('staff_bonus_discipline.delete',[$staff_bonus_discipline->id])}}"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-block" href="{{route('staff_bonus_discipline.edit',[$staff_bonus_discipline->id])}}" > <i class="fas fa-pen"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
           </div>
        </div>
    </div>
</section>  

@stop

