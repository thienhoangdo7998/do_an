@extends('layout.admin')

@section('module_user')
<section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Sửa Thông Tin Khen Thưởng - Kỉ Luật</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Khen Thưởng - Kỉ Luật</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
 <div class="container-fluid">
    <div class="row">
          <!-- left column -->
      <div class="col-md-12">
            <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thông Tin Khen Thưởng - Kỉ Luật</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" method="post" action="">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="id" value="{{$staff_bonus_discipline->id}}">
            <div class="card-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-12 col-md-4">
                    <label>Tên Nhân Viên</label>
                    <select  class="select form-control" name="id_staff" >
                      <?php if(isset($staffs)): ?>
                      <?php foreach ($staffs as $ds): ?>
                          <option value="{{$ds->id}}">{{$ds->Ho_Ten}}</option>
                      <?php endforeach; ?>     
                      <?php endif; ?> 
                    </select> 
                  </div>

                  <div class="col-12 col-md-4">
                    <label>Hình Thức</label>
                    <select  class="select form-control" name="id_bonus_discipline" >
                      <option value="" disabled selected>Hãy Chọn Hình Thức</option>
                      <?php if(isset($bonus_disciplines)): ?>
                      <?php foreach ($bonus_disciplines as $ds): ?>
                          <option value="{{$ds->id}}">{{$ds->Hinh_Thuc}}</option>
                      <?php endforeach; ?>     
                      <?php endif; ?> 
                    </select> 
                  </div>
                </div>

                <div class="row">
                   <div class="col-12 col-md-6">
                    <label>Ngày Quyết Định</label>
                    <div class="input-group mb-5">
                      <div class="input-group date"  id="reservationdate1" data-target-input="nearest">
                          <input type="text" placeholder="dd/mm/YYYY" value="{{ date('d-m-Y',strtotime(str_replace('/','-',$staff_bonus_discipline->Ngay_QD)))}}" class="form-control datetimepicker-input" data-target="#reservationdate1"/>
                          <div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                   <div class="col-12 col-md-12">
                     <label>Số Quyết Định</label>
                      <div class="input-group mb-3" style="align-content: center;">
                        <input type="text" name="So_Quyet_Dinh" value="{{$staff_bonus_discipline->So_Quyet_Dinh}}" class="form-control" placeholder="Hình Thức">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-users"></span>
                            </div>
                        </div>
                     </div> 
                   </div>            
                 </div>

                 <div class="row">
                   <div class="col-12 col-md-12">
                     <label>Lý Do</label>
                      <div class="input-group mb-3" style="align-content: center;">
                        <input type="text" name="Ly_Do" value="{{$staff_bonus_discipline->Ly_Do}}" class="form-control" placeholder="Hình Thức">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-users"></span>
                            </div>
                        </div>
                     </div> 
                   </div>            
                 </div>
              </div>   
            </div>
              <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              </div>
            <!-- /.card-body -->
          </form>
      </div>
    </div>
  </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
  $(function () {

    //Date range picker
    $('#reservationdate1').datetimepicker({
        format: 'L'
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
@endsection
 