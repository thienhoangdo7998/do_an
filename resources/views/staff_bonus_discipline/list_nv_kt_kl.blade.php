@extends('layout.admin')

@section('module_user')
<section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thông Tin Nhân Viên Khen Thưởng - Kỉ Luật</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Nhân Viên Khen Thưởng - Kỉ Luật</li>
                 </ol>
                </div>
              </div>
            </div>
</section>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">      
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                    <th style="text-align: center; vertical-align: middle;">Hình Thức</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngày Quyết Định</th>
                                    <th style="text-align: center; vertical-align: middle;">Lý Do</th>
                                    <th style="text-align: center; vertical-align: middle;">Không Xác Nhận</th>
                                    <th style="text-align: center; vertical-align: middle;">Xác Nhận</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($staff_bonus_disciplines)): ?>
                                <?php $i = 1;?>
                                <?php foreach ($staff_bonus_disciplines as $staff_bonus_discipline): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$i++}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$staff_bonus_discipline->Ho_Ten}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$staff_bonus_discipline->Hinh_Thuc}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{date('d-m-Y',strtotime(str_replace('/','-',$staff_bonus_discipline->Ngay_QD)))}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$staff_bonus_discipline->Ly_Do}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{route('staff_bonus_discipline.cancel_level_up',[$staff_bonus_discipline->id])}}"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-block" href="{{route('staff_bonus_discipline.level_up',[$staff_bonus_discipline->id])}}" > <i class="fas fa-pen"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@stop