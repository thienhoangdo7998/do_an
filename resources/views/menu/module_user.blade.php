@extends('layout.admin')

@section('module_user')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tài Khoản Người Dùng</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Tài Khoảng Người Dùng</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
        <a href="{{ route('create') }}" class="btn btn-primary btn-flat" style="margin: 0px 0px 0px 10px;border-radius: 10px">
            <i class="fas fa-plus"></i> {{ trans('Thêm Tài Khoản Người Dùng') }}
            </a>
        </a>
    </div>
<!-- <div class="container-fluid">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">
                        <form method="post" enctype="multipart/form-data" action="{{route('import_execl')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <table class="table">
                                    <tr>
                                        <td width="40%" align="rigth">
                                            <label>Select File for Upload</label>
                                        </td>
                                        <td width="30%" align="left">
                                            <input type="File" name="select_file">
                                        </td>
                                        <td width="30%" align="left">
                                            <input type="submit" name="uploadfile" class="btn btn-primary" value="Upload">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
   </div>  
</div> -->


<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">
                             
                    <table class="table table-bordered table-hover" >
                        <thead>
                            <tr>
                              	<th style="text-align: center; vertical-align: middle;">Id</th>
                                <th style="text-align: center; vertical-align: middle;">Tên</th>
                                <th style="text-align: center; vertical-align: middle;">Email</th>
                                <th style="text-align: center; vertical-align: middle;">Phân Quyền</th>
                                <th style="text-align: center; vertical-align: middle;">Chức Năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($users)): ?>
                            <?php $i = 1;?>
                            <?php foreach ($users as $user): ?>
                            	<tr>
                            		<td style="text-align: center; vertical-align: middle;">
                            			<a href="#">{{$i++}}</a>
                            		</td>

                            		<td style="text-align: center; vertical-align: middle;">
                            			<a href="#">{{$user->name}}</a>
                            		</td>
                            		<td style="text-align: center; vertical-align: middle;">
                            			<a href="#">{{$user->email}}</a>	
                            		</td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$user->type}}</a>    
                                    </td>
                            		<td style="text-align: center; vertical-align: middle;">
                                        <div class="btn-group">
                                            <a class="btn btn-danger" href="{{route('delete_user',[$user->id])}}"><i class="fa fa-trash"></i> Delete</a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-primary btn-block" href="{{route('edit',[$user->id])}}"> <i class="fas fa-pen"></i> update</a>
                                        </div>
                                    </td>
                            	</tr>
                            <?php endforeach; ?>
                            <?php endif; ?>	
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> 

<!-- <script type="text/javascript">

    $(document).ready(function($) {
        var engine1 = new Bloodhound({
            remote: {
                url: '/search/name?value=%QUERY%',
                wildcard: '%QUERY%'
            },
            datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });

        var engine2 = new Bloodhound({
            remote: {
                url: '/search/email?value=%QUERY%',
                wildcard: '%QUERY%'
            },
            datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });

        $(".search-input").typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, [
            {
                source: engine1.ttAdapter(),
                name: 'students-name',
                display: function(data) {
                    return data.name;
                },
                templates: {
                    empty: [
                        '<div class="header-title">Name</div><div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
                    ],
                    header: [
                        '<div class="header-title">Name</div><div class="list-group search-results-dropdown"></div>'
                    ],
                    suggestion: function (data) {
                        return '<a href="/students/' + data.id + '" class="list-group-item">' + data.name + '</a>';
                    }
                }
            }, 
            {
                source: engine2.ttAdapter(),
                name: 'students-email',
                display: function(data) {
                    return data.email;
                },
                templates: {
                    empty: [
                        '<div class="header-title">Email</div><div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
                    ],
                    header: [
                        '<div class="header-title">Email</div><div class="list-group search-results-dropdown"></div>'
                    ],
                    suggestion: function (data) {
                        return '<a href="/students/' + data.id + '" class="list-group-item">' + data.email + '</a>';
                    }
                }
            }
        ]);
    });
</script> -->

@stop