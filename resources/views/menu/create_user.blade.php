@extends('layout.admin')

@section('module_user')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">
                       <div class="box-body">
                        <form class="col-md-8" method="post" action="">
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
        
                              <div class="input-group mb-3" style="width: 80%;">
                                <input type="text" name="name" class="form-control" placeholder="Name">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-users"></span>
                                  </div>
                                </div>
                              </div>

                              <div class="input-group mb-3" style="width: 80%;">
                                <input type="email" name="email" class="form-control" placeholder="Email">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                  </div>
                                </div>
                              </div>

                              <div class="input-group mb-3" style="width: 80%;">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                  </div>
                                </div>
                              </div>


                               <div class="form-group">
                                  <select class="select2" multiple="multiple" data-placeholder="Select a State" style="width: 80%;" name="roles[]">
                                    @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->display_name}}</option>
                                    @endforeach
                                  </select>
                                </div>

                                <div class="col-4" >
                                  <button type="submit" class="btn btn-primary btn-block">Thêm Người Dùng</button>
                                </div>
                            </form>
                        </div>                  
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script src="{{asset('adminlte/plugins/select2/js/select2.full.min.js')}}"></script>

<script type="text/javascript">

      $("#nameid").select2({
            placeholder: "Chọn một dịch vụ",
            allowClear: true
        });
</script>
<script language="javascript">
    $("select2").mousedown(function(e) {
      e.preventDefault();

      var select = this;
      var scroll = select.scrollTop;

      e.target.selected = !e.target.selected;

      setTimeout(function() {
        select.scrollTop = scroll;
      }, 0);

      $(select).focus();
    }).mousemove(function(e) {
      e.preventDefault()
    });

    $('.select2').select2()

      
</script>
@endsection