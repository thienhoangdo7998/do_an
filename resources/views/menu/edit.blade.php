@extends('layout.admin')

@section('module_user')

 <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">
                       <div class="box-body">
                          <form  class="col-md-8" method="post" action="">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" value="{{$user->id}}">
                            
                              <div class="input-group mb-3" style="width: 80%;">
                                <input type="text" name="ten" class="form-control" placeholder="{{$user->name}}" value="{{$user->name}}">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-users"></span>
                                  </div>
                                </div>
                              </div>

                              <div class="input-group mb-3" style="width: 80%;">
                                <input type="email" name="email" class="form-control" placeholder="{{$user->email}}" value="{{$user->email}}">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                  </div>
                                </div>
                              </div>


                              
                                <div class="form-group">
                                  <select class="select2" multiple="multiple" data-placeholder="Select a State" style="width: 80%;" name="roles[]">
                                    @foreach($roles as $role)
                                    <option value="{{$role->id}}" {{$listroles->contains($role->id) ? 'selected' :  ''}}>{{$role->display_name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              

                                <div class="col-4">
                                  <button type="submit" class="btn btn-primary btn-block">Update</button>
                                </div>

                              </div>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script src="{{asset('adminlte/plugins/select2/js/select2.full.min.js')}}"></script>

<script type="text/javascript">

      $("#nameid").select2({
            placeholder: "Chọn một dịch vụ",
            allowClear: true
        });
</script>
<script language="javascript">
    $("select2").mousedown(function(e) {
      e.preventDefault();

      var select = this;
      var scroll = select.scrollTop;

      e.target.selected = !e.target.selected;

      setTimeout(function() {
        select.scrollTop = scroll;
      }, 0);

      $(select).focus();
    }).mousemove(function(e) {
      e.preventDefault()
    });

    $('.select2').select2()

      
</script> 

@endsection