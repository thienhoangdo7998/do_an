@extends('layout.admin')

@section('module_user')


<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">
                        <div class="box-body">
                          <form  class="col-md-8" method="post" action="">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" value="{{$role->id}}">

                              <div class="input-group mb-3" style="width: 80%;">
                                <input type="text" name="name" class="form-control" placeholder="{{$role->name}}" value="{{$role->name}}">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-users"></span>
                                  </div>
                                </div>
                              </div>

                              <div class="input-group mb-3" style="width: 80%;">
                                <input type="text" name="display_name" class="form-control" placeholder="{{$role->display_name}}" value="{{$role->display_name}}">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                 </div>
                                </div>
                              </div>

                              <?php if (isset($permissions)): ?>
                              <?php foreach ($permissions as $permission): ?>
                                  <div class="custom-control custom-checkbox">
                                      <input class="custom-control-input" type="checkbox" name="permission[]" id="{{$permission->id}}" value="{{$permission->id}}" {{ $listpermissions->contains($permission->id) ? 'checked' : '' }} >
                                      <label for="{{$permission->id}}" class="custom-control-label">{{$permission->display_name}}</label>
                                  </div>
                              <?php endforeach; ?>
                              <?php endif; ?>

                                <div class="col-4">
                                  <button type="submit" class="btn btn-primary btn-block">Update</button>
                                </div>
          <!-- /.col -->        </form>
                            </div>
                      </div>  
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@endsection