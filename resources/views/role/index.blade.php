@extends('layout.admin')

@section('content')
<div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{route('create_role')}}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fas fa-pencil"></i> {{ trans('Thêm Tài Khoản Người Dùng') }}
                    </a>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle;">Id</th>
                                <th style="text-align: center; vertical-align: middle;">Tên</th>
                                <th style="text-align: center; vertical-align: middle;">Display name</th>
                                <th style="text-align: center; vertical-align: middle;">Chức Năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($roles)): ?>
                                <?php $i = 1;?>
                            <?php foreach ($roles as $role): ?>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$i++}}</a>
                                    </td>

                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$role->name}}</a>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="#">{{$role->display_name}}</a> 
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <div class="btn-group">
                                            <a class="btn btn-danger" href="{{route('delete_role',[$role->id])}}"><i class="fa fa-trash"></i> Delete</a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-primary btn-block" href="{{route('edit_role',[$role->id])}}"> <i class="fas fa-pen"></i> update</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php endif; ?> 
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@stop
