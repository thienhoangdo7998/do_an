@extends('layout.admin')

@section('content')
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">
                        <form class="col-md-8" method="post" action="">
                          <input type="hidden" name="_token" value="{{csrf_token()}}">
        
                          <div class="input-group mb-3" style="width: 80%;">
                            <input type="text" name="name" class="form-control" placeholder="Name">
                            <div class="input-group-append">
                              <div class="input-group-text">
                                <span class="fas fa-users"></span>
                              </div>
                            </div>
                          </div>

                          <div class="input-group mb-3" style="width: 80%;">
                            <input type="text" name="display_name" class="form-control" placeholder="Display name">
                            <div class="input-group-append">
                              <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                              </div>
                            </div>
                          </div>
                          <?php if (isset($permissions)): ?>
                          <?php foreach ($permissions as $permission): ?>
                              <div class="custom-control custom-checkbox">
                                  <input class="custom-control-input" type="checkbox" id="{{$permission->id}}" value="{{$permission->id}}" name="permission[]">
                                  <label for="{{$permission->id}}" class="custom-control-label">{{$permission->display_name}}</label>
                              </div>
                          <?php endforeach; ?>
                          <?php endif; ?>
                            <div class="col-4" >
                              <button type="submit" class="btn btn-primary btn-block">Thêm </button>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@endsection