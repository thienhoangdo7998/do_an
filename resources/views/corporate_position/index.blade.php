@extends('layout.admin')

@section('content')
<section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Thông Tin Chức Vụ Đoàn</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Thông Tin Chức Vụ Đoàn</li>
                 </ol>
                </div>
              </div>
            </div>
          </section>
<div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
    <a href="{{route('corporate_position.create')}}" class="btn btn-primary btn-flat" style="margin: 0px 0px 0px 10px;border-radius: 10px">
        <i class="fas fa-plus"></i> {{ trans('Thêm chức Vụ Đoàn') }}
    </a>
</div>

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Danh Sách Chức Vụ Đoàn</h3>
                </div>
            <!-- /.card-header -->
                <div class=" card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên</th>
                                    <th style="text-align: center; vertical-align: middle;">Xóa</th>
                                    <th style="text-align: center; vertical-align: middle;">Sửa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($corporate_positions)): ?>
                                    <?php $i = 1;?>
                                <?php foreach ($corporate_positions as $corporate_position): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="#">{{$i++}}</a>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="{{route('corporate_position.listnv',[$corporate_position->id])}}">{{$corporate_position->TenChucvuDoan}}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{route('corporate_position.delete',[$corporate_position->id])}}"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-block" href="{{route('corporate_position.edit',[$corporate_position->id])}}" > <i class="fas fa-pen"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
           </div>
        </div>
    </div>
</section>            

@stop

