@extends('layout.admin')

@section('module_user')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Thay Đổi Chức Vụ Đoàn</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">  
         </ol>
        </div>
      </div>
    </div>
</section>
 <div class="container-fluid">
    <div class="row">
          <div class="col-12">
                <div class="card">
                  <div class="card-header">
                        <h3 class="card-title">Thay Đổi Chức Vụ Đoàn</h3>
                  </div>
                  <div class="card-body">
                      <form class="form-group col-12" method="POST"  action="{{route('corporate_position.update_corporate')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}";>
                        <div class="row">
                            <div class="col-md-3">
                                <select class="select form-control" name="Ho_Ten" id="select2">
                                    <option value="" disabled selected>Hãy Chọn Tên Nhân Viên</option>
                                    <?php if(isset($personal_informations)): ?>
                                    <?php foreach ($personal_informations as $ds): ?>
                                        <option value="{{$ds->Ma_NV}}">{{$ds->Ho_Ten}}</option>
                                    <?php endforeach; ?>     
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="select form-control" name="CVDoan" id="select2">
                                    <option value="" disabled selected>Hãy Chọn Chức Vụ Đoàn</option>
                                    <?php if(isset($corporate_positions)): ?>
                                    <?php foreach ($corporate_positions as $ds): ?>
                                        <option value="{{$ds->id}}">{{$ds->TenChucvuDoan}}</option>
                                    <?php endforeach; ?>     
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary btn">
                                    <i class="fas fa-pen"></i> Thay Đổi Chức Đoàn
                                </button>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body pad table-responsive">
                    <div class="table-responsive">      
                        <table class="table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Nhân Viên</th>
                                    <th style="text-align: center; vertical-align: middle;">Tên Chức Vụ Đoàn</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($personal_informations)): ?>
                                <?php $i = 1;?>
                                <?php foreach ($personal_informations as $staff_bonus_discipline): ?>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$i++}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$staff_bonus_discipline->Ho_Ten}}</p>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <p href="#">{{$staff_bonus_discipline->TenChucvuDoan}}</p>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@stop