
<div class="login-box">
  <div class="login-logo">
    <b>Quản Lý Cán Bộ</b>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Hãy Đăng Nhập Để Bắt Đầu Sử Dụng</p>

      <form method="post" action="">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Nhập Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>

        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Nhập Mật Khẩu">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-6">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Nhớ Mật Khẩu
              </label>
            </div>
          </div>

          <!-- /.col -->
          <div class="col-6">
            <button type="submit" class="btn btn-primary btn-block">Đăng Nhập</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->

      <p class="mb-1">
        <a href="{{route('forgotpassword')}}">Thay Đổi Mật Khẩu</a>
      </p>
    </div>
      @if(count($errors) > 0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $err)
      {{$err}}<br>
    @endforeach
  </div>
  @endif
  @if(isset($message))
    <p style="color: red">
      {{ $message}}
    </p>
  @endif
  </div>
</div>
