<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableStaffBonusDiscipline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_bonus_discipline', function (Blueprint $table) {
            $table->id();
            $table->integer('id_staff');
            $table->integer('id_bonus_discipline');
            $table->string('Ly_Do');
            $table->date('Ngay_QD');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_bonus_discipline');
    }
}
