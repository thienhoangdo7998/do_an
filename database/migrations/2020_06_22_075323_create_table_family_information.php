<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFamilyInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_information', function (Blueprint $table) {
            $table->id();
            $table->string('Ho_Ten_Cha');
            $table->string('Ho_Ten_Me');
            $table->string('Nghe_Nghiep_Cha');
            $table->string('Nghe_Nghiep_Me');
            $table->string('Noi_Sinh_Cha');
            $table->string('Noi_Sinh_Me');

            $table->string('Ho_Ten_Vo/Chong');
            $table->string('Nghe_Nghiep_Vo/Chong');
            $table->string('Noi_Sinh_Vo/Chong');
            $table->string('Ho_Ten_Con1');
            $table->string('Nghe_Nghiep_Con1');
            $table->string('Ho_Ten_Con2');
            $table->string('Nghe_Nghiep_Con2');
            $table->string('Ho_Ten_Con3');
            $table->string('Nghe_Nghiep_Con3');

            $table->integer('id_staff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_information');
    }
}
