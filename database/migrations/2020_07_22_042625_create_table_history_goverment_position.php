<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableHistoryGovermentPosition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_goverment_position', function (Blueprint $table) {
            $table->id();
            $table->string('Ma_NV');
            $table->date('Ngay_Nhan_Chuc');
            $table->integer('id_goverment_position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_goverment_position');
    }
}
