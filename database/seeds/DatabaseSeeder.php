<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $data = [
       	'name' => 'thien',
       	'email' => 'clinic@gmail.com',
       	'email_verified_at' => Null,
       	'password' => bcrypt('123456'),
       	'remember_token' => null,
       	'created_at' =>new Datatime(),
       ];
       DB::table('users') ->insert($data);
    }
}
