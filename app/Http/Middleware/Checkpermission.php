<?php

namespace App\Http\Middleware;
use App\permission;
use Closure;
use DB;

class Checkpermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = null)
    {
        $listroleuser = DB::table('users')
        ->join('role_user','users.id','=','role_user.id_user')
        ->join('role','role.id','=','role_user.id_role')
        ->where('users.id',auth()->id())
        ->select('role.*')
        ->get()->pluck('id')->toArray();

         $listroleuser = DB::table('role')
        ->join('permission_role','role.id','=','permission_role.id_role')
        ->join('permission','permission.id','=','permission_role.id_permission')
        ->where('role.id',$listroleuser)
        ->select('permission.*')
        ->get()->pluck('id')->unique();

       $Checkpermission = permission::where('name', $permission)->value('id');
       if($listroleuser->contains($Checkpermission))
       {
            return $next($request);
       }
       return back()->witherror('Bạn Không Quyền Vào Trang Này');
    }
}
