<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\goverment_position;
use DB;
use Hash;
use App\staff;
use App\personal_information;
use App\history_goverment_position;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use Carbon\Carbon;

class Controllergoverment_position extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $personal_information;
    private $goverment_position;
    private $history_goverment_position;
    public function __construct(goverment_position $goverment_position,history_goverment_position $history_goverment_position,personal_information $personal_information)
    {
        $this->goverment_position = $goverment_position; 
        $this->history_goverment_position = $history_goverment_position;
    }
    public function index()
    {

        $goverment_positions = $this->goverment_position->all();
        return view('goverment_position.index', compact('goverment_positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('goverment_position.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try
        {
            DB::beginTransaction();
            $ar = array('đ' => 'd','ủ' => 'u','Đ' => 'd');
            $bien = strtr($request->tencvcq, $ar);
            $arr = explode(" ",$bien);
            $newArray = array(); 
            foreach($arr as $value) 
            { 
                
                $newArray[] = $value[0];
                $string = implode($newArray);
            }
            $list = new goverment_position;
            $list->Ma_CVCQ = strtoupper($string);
            $list->TenChucvuCQ = $request->tencvcq;
            $list->save();   
            DB::commit();

            return Redirect()->route('goverment_position.index')->withSuccess('Thêm Thành Công','Item created successfully!');
        }

        catch(\Exception $exception)
        {
            return Redirect()->route('goverment_position.index')->witherror('Thêm Không Thành Công!');
        }
        
/*        $ar = array('đ' => 'd');
            $bien = strtr($request->tencvcq, $ar);
            $arr = explode(" ",$bien);
            $newArray = array(); 
            foreach($arr as $value) 
            { 
                
                $newArray[] = $value[0];
                $string = implode($newArray);
            }*/

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(goverment_position $goverment_position)
    {
        return view('goverment_position.edit',compact('goverment_position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try
         {
            $ar = array('đ' => 'd','ủ' => 'u','Đ' => 'd','U' => 'u');
            $bien = strtr($request->tencvcq, $ar);
            $arr = explode(" ",$bien);
            $newArray = array(); 
            foreach($arr as $value) 
            { 
                
                $newArray[] = $value[0];
                $string = implode($newArray);
            }

            DB::beginTransaction();
                $list = goverment_position::find($request->input('id'));
                $list->Ma_CVCQ = strtoupper($string);
                $list->TenChucvuCQ = $request->input('tencvcq');
                $list->save();
                
            DB::commit();
            return Redirect()->route('goverment_position.index')->withSuccess('Sửa Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('goverment_position.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(goverment_position $goverment_position)
    {
        

        $fk = DB::table('staff')->where('staff.goverment_position_id','=',$goverment_position->id)->get();
        if(empty($fk) == true)
        {
            $goverment_positions = $this->goverment_position->find($goverment_position->id);
            $goverment_positions->delete($goverment_position->id);
            return redirect()->route('goverment_position.index')->withSuccess('success','Item created successfully!');
        }
        else
        {
           return redirect()->route('goverment_position.index')->witherror('Vẫn Còn Cán Bộ Trong Phòng Ban Này!','Item created successfully!');
        }
    }

    public function listnv(goverment_position $goverment_position)
    {
        $goverment_positions = $this->goverment_position->find($goverment_position->id);
        $listnv = DB::table('goverment_position')
                    ->join('staff','goverment_position.id','=','staff.goverment_position_id')
                    ->where('staff.status','=',0)
                    ->where('goverment_position.id','=',$goverment_positions->id)
                    ->select('Ma_NV','Ho_Ten','TenChucvuCQ')->get();

        if($listnv->isEmpty())
        {
           return back()->witherror('Không Có Nhân Viên Thuộc Chức Vụ Này');
        }
        else
        {
           return view('goverment_position.list_nv', compact('listnv')); 
        }
    }
    public function update_goverment()
    {
        $personal_informations = DB::table('staff')->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')->get();
        $goverment_positions = $this->goverment_position->all();
        return view('goverment_position.update_goverment',compact('personal_informations','goverment_positions'));
    }
    public function post_update_goverment(request $request)
    {
        try
         {
            DB::beginTransaction();
                
                $list1 = new history_goverment_position;
                $bien = DB::table('staff')->where('Ma_NV','=', $request->Ho_Ten)->get();
                $list = personal_information::find($bien[0]->id);
                $list->goverment_position_id = $request->CVDang;
                $list1->Ma_NV = $request->Ho_Ten;
                $list1->id_goverment_position = $request->CVDang;
                $list1->Ngay_Nhan_Chuc = Carbon::now();
                $list->save();
                $list1->save();
                
            DB::commit();
            return Redirect()->route('goverment_position.update_goverment')->withSuccess('Thay Đổi Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('goverment_position.update_goverment')->witherror('Thay Đổi Không Thành Công!');
        }
    }
}
