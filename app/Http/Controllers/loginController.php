<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use App\corporate_position;
use App\party_position;
use App\goverment_position;
use App\rank;
use App\major;
use App\personal_information;
use App\staff_bonus_discipline;
use DB;
use Hash;

class loginController extends Controller
{
	private $personal_information;
	private $user;
    public function __construct(personal_information $personal_information,user $user)
    {
        $this->personal_information = $personal_information;
        $this->user = $user; 
    }
	public function validator(array $data)
	{
		return Validator::make($data,[
			'email' => 'required|email|max:225',
			'password' => 'required|min:0',
		]);
	}

	public function login(Request $request)
	{

		if($request -> isMethod('post'))
		{
			$email = $request->input('email');
			$password = $request->input('password');
			$validator = $this->validator($request->all());
			if($validator->fails()){
				return Redirect::to("/login")->withInput()->withErrors($validator);
			}
			if(Auth::attempt(['email' => $email,'password' => $password])){
				if(Auth::user() && Auth::user()->isAdmin()){
					return Redirect::to("/");
				}
				else
				{
					return redirect()->route('home',[$email]);
				}
			}
			else
			{
				return Redirect::to("/login")->withInput()->withErrors("Email hoặc mật khẩu chưa đúng");
			}
			return back()->withInput();
		}
		return view("login.layoutlogin");
	} 
	public function logout()
	{
		if(Auth::logout()){
			return Redirect('/login.layoutlogin');
		}
		return Redirect('/');
	}
	public function index(Request $request)
	{
		$bien = DB::table('users')->where('email','=', $request->email)->get();
    	$personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('Ma_NV','=',$bien[0]->Ma_NV_id)
         ->select('staff.id','Ma_NV','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ten_trinh_do','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam','Avatar','Luong_CB','Ngay_Len_Ham')
         ->get();
         $family_informations = DB::table('family_information')
         ->join('staff','family_information.id_staff','=','staff.id')
         ->where('Ma_NV','=',$bien[0]->Ma_NV_id)
         ->select('family_information.id','Ho_Ten','Ho_Ten_Cha','Ho_Ten_Me','Nghe_Nghiep_Cha','Nghe_Nghiep_Me','Noi_Sinh_Cha','Noi_Sinh_Me','Ho_Ten_Vo_Chong','Nghe_Nghiep_Vo_Chong','Noi_Sinh_Vo_Chong','Ho_Ten_Con1','Nghe_Nghiep_Con1','Ho_Ten_Con2','Nghe_Nghiep_Con2','Ho_Ten_Con3','Nghe_Nghiep_Con3',)->get();
	      return view('admin.content_home',compact('personal_informations','bien','family_informations'));
	}
}