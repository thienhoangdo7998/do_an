<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use Hash;
use DB;

class registerController extends Controller
{
	private $user;
	public function __construct(User $user)
	{
		$this->user = $user; 
	}

	public function postregister(Request $request)
	{
		try
		{
			DB::beginTransaction();
			$this->user->create([
				'name' => $request->name,
				'email' =>$request->email,
				'password' => hash::make($request->password),
			]);
			DB::commit();
			return Redirect()->route('login');
		}

		catch(\Exception $exception)
		{
			DB::rollback();
		}
		
		
	} 

	public function getregister()
	{
		return view('login.dangky');
	}

	public function forgot_password()
	{
		return view('login.forgot-password');
	}

	public function postforgot_password(Request $request)
	{
		$list = DB::table('users')->where('email','=',$request->email)->get();
		if($list->isEmpty())
		{
			return Redirect()->route('forgot-password')->witherror('Email Không Chính Xác!');	
			
		}
		else
		{
			return view('login.change_password',compact('list'));
		}
	}
	public function change_password(Request $request)
	{
		try
		{
			DB::beginTransaction();
			$list = User::find($request->id);
			$list->password = hash::make($request->password);
			$list->save();
			DB::commit();
			return Redirect()->route('login');
		}
		catch(\Exception $exception)
        {
            return Redirect()->route('login')->witherror('Sửa Không Thành Công!');
        }
	}

}