<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\family_information;
use App\staff;
use DB;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;

class ControllerFamily_information extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $family_information;
    public function __construct(family_information $family_information)
    {
        $this->family_information = $family_information; 

    }
    public function index()
    {
        $family_informations = DB::table('family_information')->join('staff','family_information.id_staff','=','staff.id')->where('staff.status','=',0)->select('family_information.id','Ho_Ten','Ho_Ten_Cha','Ho_Ten_Me','Nghe_Nghiep_Cha','Nghe_Nghiep_Me','Noi_Sinh_Cha','Noi_Sinh_Me','Ho_Ten_Vo_Chong','Nghe_Nghiep_Vo_Chong','Noi_Sinh_Vo_Chong','Ho_Ten_Con1','Nghe_Nghiep_Con1','Ho_Ten_Con2','Nghe_Nghiep_Con2','Ho_Ten_Con3','Nghe_Nghiep_Con3',)->get();

        return view('staff_information.family_information.index', compact('family_informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$staffs = DB::table('staff')->select('id','Ho_Ten')->get();
        return view('staff_information.family_information.create',compact('staffs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $list = new family_information;
            $list->Ho_Ten_Cha = $request->input('Ho_Ten_Cha');
            $list->Nghe_Nghiep_Cha = $request->input('Nghe_Nghiep_Cha');
            $list->Noi_Sinh_Cha = $request->input('Noi_Sinh_Cha');

            $list->Ho_Ten_Me = $request->input('Ho_Ten_Me');
            $list->Nghe_Nghiep_Me = $request->input('Nghe_Nghiep_Me');
            $list->Noi_Sinh_Me = $request->input('Noi_Sinh_Me');

            $list->Ho_Ten_Vo_Chong = $request->input('Ho_Ten_Vo_Chong');
            $list->Nghe_Nghiep_Vo_Chong = $request->input('Nghe_Nghiep_Vo_Chong');
            $list->Noi_Sinh_Vo_Chong = $request->input('Noi_Sinh_Vo_Chong');

            $list->Ho_Ten_Con1= $request->input('Ho_Ten_Con1');
            $list->Nghe_Nghiep_Con1 = $request->input('Nghe_Nghiep_Con1');
            $list->Ho_Ten_Con2 = $request->input('Ho_Ten_Con2');
            $list->Nghe_Nghiep_Con2 = $request->input('Nghe_Nghiep_Con2');
            $list->Ho_Ten_Con3 = $request->input('Ho_Ten_Con3');
            $list->Nghe_Nghiep_Con3 = $request->input('Nghe_Nghiep_Con3');

            $list->id_staff = $request->input('staff_id');
            $list->save();   
            DB::commit();

            return Redirect()->route('family_information.index')->withSuccess('Thêm Thành Công','Item created successfully!');
        }

        catch(\Exception $exception)
        {
            return Redirect()->route('family_information.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(family_information $family_information)
    {

        return view('staff_information.family_information.edit',compact('family_information'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try
         {
            DB::beginTransaction();
                $list = family_information::find($request->input('id'));
                $list->Ho_Ten_Cha = $request->input('Ho_Ten_Cha');
	            $list->Nghe_Nghiep_Cha = $request->input('Nghe_Nghiep_Cha');
	            $list->Noi_Sinh_Cha = $request->input('Noi_Sinh_Cha');

	            $list->Ho_Ten_Me = $request->input('Ho_Ten_Me');
	            $list->Nghe_Nghiep_Me = $request->input('Nghe_Nghiep_Me');
	            $list->Noi_Sinh_Me = $request->input('Noi_Sinh_Me');

	            $list->Ho_Ten_Vo_Chong = $request->input('Ho_Ten_Vo_Chong');
	            $list->Nghe_Nghiep_Vo_Chong = $request->input('Nghe_Nghiep_Vo_Chong');
	            $list->Noi_Sinh_Vo_Chong = $request->input('Noi_Sinh_Vo_Chong');

	            $list->Ho_Ten_Con1= $request->input('Ho_Ten_Con1');
	            $list->Nghe_Nghiep_Con1 = $request->input('Nghe_Nghiep_Con1');
	            $list->Ho_Ten_Con2 = $request->input('Ho_Ten_Con2');
	            $list->Nghe_Nghiep_Con2 = $request->input('Nghe_Nghiep_Con2');
	            $list->Ho_Ten_Con3 = $request->input('Ho_Ten_Con3');
	            $list->Nghe_Nghiep_Con3 = $request->input('Nghe_Nghiep_Con3');

	            $list->id_staff = $request->input('staff_id');  

                $list->save();
                
            DB::commit();
            return Redirect()->route('family_information.index')->withSuccess('Sửa Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('family_information.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(family_information $family_information)
    {
        $family_informations = $this->family_information->find($family_information->id);
        $family_informations->delete($family_information->id);
        return redirect()->route('family_information.index')
            ->withSuccess('success','Item created successfully!');

    }
}
