<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use App\User;
use App\role;
use App\Exports\user_Export;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Imports\UsersImport;
use Excel;

class UserController extends Controller
{

    private $user;
    private $role;
    public function __construct(User $user,role $role)
    {
        $this->user = $user; 
        $this->role = $role; 
    }
    
    public function index()
    {
        $users = $this->user->all();
        return view('menu.module_user',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->role->all();
        return view('menu.create_user',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $list = new user;
            $list->name = $request->input('name');
            $list->email = $request->input('email');
            $list->password = Hash::make($request->input('password'));
            $list->save();   
            $list->Roles_()->attach($request->roles);
            DB::commit();

            return Redirect()->route('user')->withSuccess('success','Item created successfully!');
        }

        catch(\Exception $exception)
        {
            DB::rollback();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = $this->role->all();
        $listroles = DB::table('role_user')->where('id_user', $user->id )->pluck('id_role');
        return view('menu.edit',compact('user','roles','listroles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         try
         {
            DB::beginTransaction();
                $list = User::find($request->input('id'));
                $list->name = $request->input('ten');
                $list->email = $request->input('email');
                $list->save();
                
                DB::table('role_user')->where('id_user',$request->id)->delete();
                
                $list->Roles_()->attach($request->roles);
              

            DB::commit();
            return Redirect()->route('user')->withSuccess('success','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        
        $users = $this->user->find($user->id);
        $users->delete($user->id);
        $users->Roles_()->detach();
        return redirect()->route('user')
            ->withSuccess('success','Item created successfully!');
    }
    public function export() 
    {
        
        return Excel::download(new user_Export , 'invoices.xlsx');
    }

    public function search(Request $request)
    {
         $users = User::where('name','like','%'.$request->value.'%')
                        ->orwhere('email','like','%'.$request->value.'%')
                        ->select('name','email','id')
                        ->get();
        return view('menu.module_user',compact('users'));
    }

    public function import(Request $request) 
    {
        $request->validate([
            'file' => 'required',
        ]);
        if($request->hasFile('select_file'))
        {
            Excel::import(new UsersImport, $request->select_file);
        }
        return redirect()->route('user')->with('success', 'All good!');
    }
    
}
