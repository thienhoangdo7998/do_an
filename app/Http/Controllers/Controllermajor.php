<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\major;
use DB;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use Carbon\Carbon;

class Controllermajor extends Controller
{
    private $major;
    public function __construct(major $major)
    {
        $this->major = $major; 

    }
    public function index()
    {
        $majors = $this->major->all();
        return view('major.index', compact('majors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('major.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $list = new major;
            $list->Ten_Nghiep_Vu = $request->input('ten_nghiep_vu');
            $list->save();   
            DB::commit();

            return Redirect()->route('major.index')->withSuccess('Thêm Thành Công','Item created successfully!');
        }

        catch(\Exception $exception)
        {
            return Redirect()->route('major.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(major $major)
    {
        return view('major.edit',compact('major'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try
         {
            DB::beginTransaction();
                $list = major::find($request->input('id'));
                $list->Ten_Nghiep_Vu = $request->input('tencvcq');
                $list->save();
                
            DB::commit();
            return Redirect()->route('major.index')->withSuccess('Sửa Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('major.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(major $major)
    {
        $majors = $this->major->find($major->id);
        $majors->delete($major->id);
        return redirect()->route('major.index')
            ->withSuccess('success','Item created successfully!');

    }
}
