<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\corporate_position;
use DB;
use Hash;
use App\history_corporate_position;
use App\personal_information;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use Carbon\Carbon;

class Controllercorporate_position extends Controller
{
    private $corporate_position;
    private $personal_information;
    private $history_corporate_position;
    public function __construct(corporate_position $corporate_position,personal_information $personal_information,history_corporate_position $history_corporate_position)
    {
        $this->corporate_position = $corporate_position; 
        $this->personal_information = $personal_information;
        $this->history_corporate_position = $history_corporate_position;

    }
    public function index()
    {
        $corporate_positions = $this->corporate_position->all();
        return view('corporate_position.index', compact('corporate_positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('corporate_position.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $list = new corporate_position;
            $ar = array('đ' => 'd','ủ' => 'u','Đ' => 'd','U' => 'u','Ủ'=>'u');
            $bien = strtr($request->tencvdoan, $ar);
            $arr = explode(" ",$bien);
            $newArray = array(); 
            foreach($arr as $value) 
            { 
                
                $newArray[] = $value[0];
                $string = implode($newArray);
            }
            $list->Ma_CVD = strtoupper($string);
            $list->TenChucvuDoan = $request->input('tencvdoan');
            $list->save();   
            DB::commit();

            return Redirect()->route('corporate_position.index')->withSuccess('Thêm Thành Công','Item created successfully!');
        }

        catch(\Exception $exception)
        {
            return Redirect()->route('corporate_position.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(corporate_position $corporate_position)
    {
        return view('corporate_position.edit',compact('corporate_position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try
         {
            DB::beginTransaction();
                $list = corporate_position::find($request->input('id'));
                $ar = array('đ' => 'd','ủ' => 'u','Đ' => 'd','U' => 'u','Ủ' => 'u');
                $bien = strtr($request->tencvdoan, $ar);
                $arr = explode(" ",$bien);
                $newArray = array(); 
                foreach($arr as $value) 
                { 
                    
                    $newArray[] = $value[0];
                    $string = implode($newArray);
                }
                $list->Ma_CVD = strtoupper($string);
                $list->TenChucvuDoan = $request->input('tencvdoan');
                $list->save();
                
            DB::commit();
            return Redirect()->route('corporate_position.index')->withSuccess('Sửa Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('corporate_position.index')->witherror('Sửa Không Thành Công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(corporate_position $corporate_position)
    {
        $fk = DB::table('staff')->where('staff.corporate_postion_id','=',$corporate_position->id)->get();
        if(empty($fk) == true)
        {
             $corporate_positions = $this->corporate_position->find($corporate_position->id);
            $corporate_positions->delete($corporate_position->id);
            return redirect()->route('corporate_position.index')->withSuccess('Xóa Thành Công!','Item created successfully!');
        }
        else
        {
           return redirect()->route('corporate_position.index')->witherror('Vẫn Còn Cán Bộ Giữ Chức Vụ Này!','Item created successfully!');
        }

    }
    public function listnv(corporate_position $corporate_position)
    {
        $corporate_positions = $this->corporate_position->find($corporate_position->id);
        $listnv = DB::table('corporate_position')
                    ->join('staff','corporate_position.id','=','staff.corporate_postion_id')
                    ->where('corporate_position.id','=',$corporate_positions->id)
                    ->where('staff.status','=',0)
                    ->select('Ma_NV','Ho_Ten','TenChucvuDoan')->get();
         return view('corporate_position.list_nv', compact('listnv','corporate_positions'));
    }
    public function update_corporate()
    {
        $personal_informations = DB::table('staff')->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')->get();
        $corporate_positions = $this->corporate_position->all();
        return view('corporate_position.update_corporate',compact('personal_informations','corporate_positions'));
    }

    public function post_update_corporate(Request $request)
    {
        try
         {
            DB::beginTransaction();
                
                $list1 = new history_corporate_position;
                $bien = DB::table('staff')->where('Ma_NV','=', $request->Ho_Ten)->get();
                $list = personal_information::find($bien[0]->id);
                $list->corporate_postion_id = $request->CVDoan;
                $list1->Ma_NV = $request->Ho_Ten;
                $list1->id_corporate_position = $request->CVDoan;
                $list1->Ngay_Nhan_Chuc = Carbon::now();
                $list->save();
                $list1->save();
                
            DB::commit();
            return Redirect()->route('corporate_position.update_corporate')->withSuccess('Sửa Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('corporate_position.update_corporate')->witherror('Sửa Không Thành Công!');
        }
         
    }
}
