<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\party_position;
use DB;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;

class Controllerparty_position extends Controller
{
    private $party_position;
    public function __construct(party_position $party_position)
    {
        $this->party_position = $party_position; 

    }
     public function index()
    {
        $party_positions = $this->party_position->all();
        return view('party_position.index',compact('party_positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view('party_position.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $list = new party_position;
            $list->TenChucvuDang = $request->input('tencvdang');
            $list->save();   
            DB::commit();

            return Redirect()->route('party_position.index')->withSuccess('Thêm Thành Công','Item created successfully!');
        }

        catch(\Exception $exception)
        {
            return Redirect()->route('party_position.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(party_position $party_position)
    {
        return view('party_position.edit',compact('party_position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         try
         {
            DB::beginTransaction();
                $list = party_position::find($request->input('id'));
                $list->TenChucvuDang = $request->input('tencvdang');
                $list->save();
                
              
            DB::commit();
            return Redirect()->route('party_position.index')->withSuccess('Sửa Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('party_position.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(party_position $party_position)
    {
        
        $fk = DB::table('staff')->where('staff.party_postion_id','=',$party_position->id)->get();
        if(empty($fk) == true)
        {
            $party_positions = $this->party_position->find($party_position->id);
            $party_positions->delete($party_position->id);
            return redirect()->route('party_position.index')->withSuccess('Xóa Thành Công!','Item created successfully!');
        }
        else
        {
           return redirect()->route('party_position.index')->witherror('Vẫn Còn Cán Bộ Giữ Chức Vụ Này!','Item created successfully!');
        }
    }

    public function listnv(party_position $party_position)
    {
        $party_positions = $this->party_position->find($party_position->id);
        $listnv = DB::table('party_position')
                    ->join('staff','party_position.id','=','staff.party_postion_id')
                    ->where('staff.status','=',0)
                    ->where('party_position.id','=',$party_position->id)
                    ->select('Ma_NV','Ho_Ten','TenChucvuDang')->get();

        if($listnv->isEmpty())
        {
           return back()->witherror('Không Có Nhân Viên Thuộc Chức Vụ Này');
        }
        else
        {
           return view('party_position.list_nv', compact('listnv')); 
        }
    }


}
