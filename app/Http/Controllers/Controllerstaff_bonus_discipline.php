<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use App\personal_information;
use App\bonus_discipline;
use App\staff_bonus_discipline;
use Carbon\Carbon;

class Controllerstaff_bonus_discipline extends Controller
{
    private $staff_bonus_discipline;
    private $personal_information;
    public function __construct(staff_bonus_discipline $staff_bonus_discipline,personal_information $personal_information)
    {
        $this->staff_bonus_discipline = $staff_bonus_discipline; 
        $this->personal_information = $personal_information;

    }

    public function index()
    {
         $staff_bonus_disciplines = DB::table('staff_bonus_discipline')
                        ->join('staff','staff_bonus_discipline.id_staff','=','staff.Ma_NV')
                        ->join('bonus_discipline','staff_bonus_discipline.id_bonus_discipline','=','bonus_discipline.id')
                        ->where('is_disable','=',0)
                        ->where('staff.status','=',0)
                        ->select('staff_bonus_discipline.id','Ho_Ten','Hinh_Thuc','Ngay_QD','So_Quyet_Dinh','Ly_Do')
                        ->get();

        return view('staff_bonus_discipline.index',compact('staff_bonus_disciplines'));
    }
    public function index_list_nv()
    {
        $time = date('Y-m-d',strtotime(str_replace('/','-',Carbon::now())));
        $staff_bonus_disciplines = DB::table('staff_bonus_discipline')
                        ->join('staff','staff_bonus_discipline.id_staff','=','staff.Ma_NV')
                        ->join('bonus_discipline','staff_bonus_discipline.id_bonus_discipline','=','bonus_discipline.id')
                        ->where('Ngay_QD','<=',$time)
                        ->where('is_disable','=',0)
                        ->where('staff.status','=',0)
                        ->select('staff_bonus_discipline.id','id_staff','Ho_Ten','id_bonus_discipline','Hinh_Thuc','Ngay_QD','So_Quyet_Dinh','Ly_Do')
                        ->get();
        return view('staff_bonus_discipline.list_nv_kt_kl',compact('staff_bonus_disciplines'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staffs = DB::table('staff')->select('id','Ho_Ten','Ma_NV')->get();
        $bonus_disciplines = DB::table('bonus_discipline')->select('id','Hinh_Thuc')->get();
        return view('staff_bonus_discipline.create',compact('staffs'),compact('bonus_disciplines'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $list = new staff_bonus_discipline;
            $list->id_bonus_discipline = $request->id_bonus_discipline;
            $list->id_staff = $request->id_staff;
            $list->Ngay_QD = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Quyet_Dinh'))));
            $list->Ly_Do = $request->Ly_Do;
            $list->So_Quyet_Dinh = $request->So_Quyet_Dinh;    
            $list->is_disable = 0;        
            $list->save();  

            DB::commit();

             return Redirect()->route('staff_bonus_discipline.create')->withSuccess('Thêm Thành Công','Item created successfully!');
        
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('staff_bonus_discipline.create')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(staff_bonus_discipline $staff_bonus_discipline)
    {
         $staffs = DB::table('staff')->select('id','Ho_Ten')->get();
         $bonus_disciplines = DB::table('bonus_discipline')->select('id','Hinh_Thuc')->get();
         return view('staff_bonus_discipline.edit',compact('staffs','bonus_disciplines','staff_bonus_discipline'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {  

         try
        {
            DB::beginTransaction();

            $bien = DB::table('staff')
                    ->where('staff.id','=',$request->id_staff)
                    ->select('rank_id')->get();
            $list = staff_bonus_discipline::find($request->input('id'));
            $list1 = personal_information::find($request->input('id_staff'));
            if($bien[0]->rank_id == 43 && ($request->id_bonus_discipline == 1 || $request->id_bonus_discipline == 2 || $request->id_bonus_discipline == 3 || $request->id_bonus_discipline == 4))
            {
                 return Redirect()->route('staff_bonus_discipline.create')->withError('Không Thể Tăng Hàm Thêm nữa!!');
            }
            
            
            if($bien[0]->rank_id == 1 && ($request->id_bonus_discipline == 5 || $request->id_bonus_discipline == 6 || $request->id_bonus_discipline == 7 || $request->id_bonus_discipline == 8))
            {
                 return Redirect()->route('staff_bonus_discipline.create')->withError('Không Thể Giảm Hàm Được Nữa!!');
            }
            
            $list->id_bonus_discipline = $request->id_bonus_discipline;
            $list->id_staff = $request->id_staff;
            $list->Ngay_QD = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_QD'))));
            $list->Ly_Do = $request->Ly_Do;
            $list->So_Quyet_Dinh = $request->So_Quyet_Dinh;            
            $list->save();  

            DB::commit();

             return Redirect()->route('staff_bonus_discipline.index')->withSuccess('Sửa Thành Công','Item created successfully!');
        
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('staff_bonus_discipline.edit')->witherror('Sửa Không Thành Công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(staff_bonus_discipline $staff_bonus_discipline)
    {
        $staff_bonus_disciplines = $this->staff_bonus_discipline->find($staff_bonus_discipline->id);
        $staff_bonus_disciplines->delete($staff_bonus_discipline->id);
        return redirect()->route('staff_bonus_discipline.index')->withSuccess('success','Item created successfully!');
    }

    public function level_up(staff_bonus_discipline $staff_bonus_discipline)
    {
        try
        {
            DB::beginTransaction();

            $bien = DB::table('staff')
                    ->where('staff.Ma_NV','=',$staff_bonus_discipline->id_staff)
                    ->select('rank_id')->get();
            $bien1 = DB::table('staff')->where('Ma_NV','=',$staff_bonus_discipline->id_staff)->get();
            $list = staff_bonus_discipline::find($staff_bonus_discipline->id);
            $list1 = personal_information::find($bien1[0]->id);
            if($bien[0]->rank_id == 43 && ($staff_bonus_discipline->id_bonus_discipline == 1 ||$staff_bonus_discipline->id_bonus_discipline == 2 || $$staff_bonus_discipline->id_bonus_discipline == 3 || $staff_bonus_discipline->id_bonus_discipline == 4))
            {
                 return Redirect()->route('staff.index')->withError('Không Thể Tăng Hàm Thêm nữa!!');
            }
            else
            {
                if($staff_bonus_discipline->id_bonus_discipline == 1){
                    $list1->rank_id = $bien[0]->rank_id + 1;
                    $list1->save();
                }
                elseif ($staff_bonus_discipline->id_bonus_discipline == 2) {
                    $list1->rank_id = $bien[0]->rank_id + 2;
                    $list1->save();
                }
                elseif ($staff_bonus_discipline->id_bonus_discipline == 3) {
                    $list1->rank_id = $bien[0]->rank_id + 3;
                    $list1->save();
                }
                elseif ($staff_bonus_discipline->id_bonus_discipline == 4) {
                    if($bien->rank_id == 1||$bien[0]->rank_id == 2)
                    {
                        $list1->rank_id = $bien[0]->rank_id + 1;
                        $list1->save();
                    }
                    elseif($bien->rank_id == 3||$bien[0]->rank_id == 4)
                    {
                        $list1->rank_id = 5;
                        $list1->save();
                    }
                    elseif ($bien[0]->rank_id == 5||$bien[0]->rank_id == 6) {
                        $list1->rank_id = 7;
                        $list1->save();
                    }
                    elseif($bien[0]->rank_id == 7||$bien[0]->rank_id == 8){
                        $list1->rank_id = 9;
                        $list1->save();
                    }
                    elseif($bien[0]->rank_id == 9||$bien[0]->rank_id == 10||$bien[0]->rank_id == 11){
                        $list1->rank_id = 12;
                        $list1->save();
                    }
                    elseif($bien[0]->rank_id == 12||$bien[0]->rank_id == 13||$bien[0]->rank_id == 14){
                        $list1->rank_id = 15;
                        $list1->save();
                    }
                    elseif($bien[0]->rank_id == 15||$bien[0]->rank_id == 16||$bien[0]->rank_id == 17||$bien[0]->rank_id == 18){
                        $list1->rank_id = 19;
                        $list1->save();
                    }
                    elseif($bien[0]->rank_id == 19||$bien[0]->rank_id == 20||$bien[0]->rank_id == 21||$bien[0]->rank_id == 22){
                        $list1->rank_id = 23;
                        $list1->save();
                    }
                    elseif($bien[0]->rank_id == 23||$bien[0]->rank_id == 24||$bien[0]->rank_id == 25||$bien[0]->rank_id == 26){
                        $list1->rank_id = 27;
                        $list1->save();
                    }
                    elseif($bien[0]->rank_id == 27||$bien[0]->rank_id == 28||$bien[0]->rank_id == 29||$bien[0]->rank_id == 30){
                        $list1->rank_id = 31;
                        $list1->save();
                    }
                    elseif($bien[0]->rank_id == 31||$bien[0]->rank_id == 32||$bien[0]->rank_id == 33||$bien[0]->rank_id == 34){
                        $list1->rank_id = 35;
                        $list1->save();
                    }
                    elseif($bien[0]->rank_id == 35||$bien[0]->rank_id == 36||$bien[0]->rank_id == 37||$bien[0]->rank_id == 38){
                        $list1->rank_id = 39;
                        $list1->save();
                    }
                    else{
                        $list1->rank_id = 43;
                        $list1->save();
                    }
                }
            }
            
            if($bien[0]->rank_id == 1 && ($staff_bonus_discipline->id_bonus_discipline == 5 || $staff_bonus_discipline->id_bonus_discipline == 6 || $staff_bonus_discipline->id_bonus_discipline == 7 || $staff_bonus_discipline->id_bonus_discipline == 8))
            {
                 return Redirect()->route('staff.index')->withError('Không Thể Giảm Hàm Được Nữa!!');
            }
            else
            {
                if($staff_bonus_discipline->id_bonus_discipline == 5){
                    $list1->stt_ky_luat = 1;
                    $list1->save();
                }
                elseif ($staff_bonus_discipline->id_bonus_discipline == 6) {
                    $list1->stt_ky_luat = 2;
                    $list1->save();
                }
                elseif ($staff_bonus_discipline->id_bonus_discipline == 7) {
                    $list1->stt_ky_luat = 3;
                    $list1->save();
                }
            }
            $list->is_disable = 1;        
            $list->save();  

            DB::commit();

            return Redirect()->route('staff.index')->withSuccess('Thăng Hàm Thành Công','Item created successfully!');
        
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('staff.index')->witherror('Thăng Hàm Không Thành Công!');
        }
    }
    public function not_level_up(staff_bonus_discipline $staff_bonus_discipline)
    {
        try
        {
            DB::beginTransaction();
            $staff_bonus_disciplines = $this->staff_bonus_discipline->find($staff_bonus_discipline->id);
            $staff_bonus_disciplines->delete($staff_bonus_discipline->id);
            DB::commit();
            return Redirect()->route('staff_bonus_discipline.show_level_up')->withSuccess('Đã Hủy Xác Nhận','Item created successfully!');

        }
         catch(\Exception $exception)
        {
            return Redirect()->route('staff_bonus_discipline.show_level_up')->witherror('Đã Có Lỗi Xảy Ra!');
        }
    }
}
