<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\rank;
use DB;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;

class Controllerrank extends Controller
{
   private $rank;
    public function __construct(rank $rank)
    {
        $this->rank = $rank; 

    }
     public function index()
    {
        $ranks = $this->rank->all();
        return view('rank.index',compact('ranks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rank.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $list = new rank;
            $list->Ten_Cap_Bac = $request->input('ten_cap_bac');
            $list->He_So_Luong = $request->input('he_so_luong');
            $list->save();   
            DB::commit();

            return Redirect()->route('rank.index')->withSuccess('Thêm Thành Công','Item created successfully!');
        }

        catch(\Exception $exception)
        {
            return Redirect()->route('rank.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(rank $rank)
    {
        return view('rank.edit',compact('rank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         try
         {
            DB::beginTransaction();
                $list = rank::find($request->input('id'));
                $list->Ten_Cap_Bac = $request->input('ten_cap_bac');
                $list->He_So_Luong = $request->input('he_so_luong');
                $list->save();
                
              
            DB::commit();
            return Redirect()->route('rank.index')->withSuccess('Sửa Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('rank.index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(rank $rank)
    {
        $ranks = $this->rank->find($rank->id);
        $ranks->delete($rank->id);
        return redirect()->route('rank.index')
            ->withSuccess('success','Item created successfully!');
    }
}
