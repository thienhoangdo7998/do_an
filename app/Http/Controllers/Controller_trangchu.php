<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use Carbon\Carbon;
use DB;
use Hash;

class Controller_trangchu extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personal_informations = DB::table('staff')->get();
        $lenham = DB::table('staff')
        ->join('rank','staff.rank_id','=','rank.id')
        ->where([
                    ['id_degree','=',1],
                    ['rank_id','<',12],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                ])
                ->orwhere([
                    ['id_degree','=',2],
                    ['rank_id','<',15],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                ])
                ->orwhere([
                    ['id_degree','=',3],
                    ['rank_id','<',19],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                ])
                ->orwhere([
                    ['id_degree','=',4],
                    ['rank_id','<',27],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                ])
                ->orwhere([
                    ['id_degree','=',5],
                    ['rank_id','<',35],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                ])
                ->orwhere([
                    ['id_degree','=',6],
                    ['rank_id','<',43],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                ])
                ->get();
        $hsl = DB::table('staff')
                ->join('rank','staff.id_rank_DT','=','rank.id')
                ->where('Ngay_Len_Ham','=>',Carbon::now())
                ->where([
                    ['id_degree','=',1],
                    ['rank_id','>=',12],
                ])
                ->orwhere([
                    ['id_degree','=',2],
                    ['rank_id','>=',15],
                ])
                ->orwhere([
                    ['id_degree','=',3],
                    ['rank_id','>=',19],
                ])
                ->orwhere([
                    ['id_degree','=',4],
                    ['rank_id','>=',27],
                ])
                ->orwhere([
                    ['id_degree','=',5],
                    ['rank_id','>=',35],
                ])
                ->orwhere([
                    ['id_degree','=',6],
                ])->get();
        $time = date('Y-m-d',strtotime(str_replace('/','-',Carbon::now())));
        $staff_bonus_disciplines = DB::table('staff_bonus_discipline')
                        ->join('staff','staff_bonus_discipline.id_staff','=','staff.Ma_NV')
                        ->join('bonus_discipline','staff_bonus_discipline.id_bonus_discipline','=','bonus_discipline.id')
                        ->where('Ngay_QD','<=',$time)
                        ->where('is_disable','=',0)
                        ->select('staff_bonus_discipline.id','id_staff','Ho_Ten','id_bonus_discipline','Hinh_Thuc','Ngay_QD','So_Quyet_Dinh','Ly_Do')
                        ->get();
        return view('layout.trangchu',compact('personal_informations','lenham','hsl','staff_bonus_disciplines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
