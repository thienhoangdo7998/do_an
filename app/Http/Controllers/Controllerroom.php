<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use App\room;

class Controllerroom extends Controller
{
    private $room;
    public function __construct(room $room)
    {
        $this->room = $room; 

    }
     public function index()
    {
        $rooms = $this->room->all();
        return view('room.index',compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('room.create_room');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try
        {
            DB::beginTransaction();
            $ar = array('đ' => 'd','ủ' => 'u','Đ' => 'd','U' => 'u');
            $bien = strtr($request->ten_pb, $ar);
            $arr = explode(" ",$bien);
            $newArray = array(); 
            foreach($arr as $value) 
            { 
                $newArray[] = $value[0];
                $string = implode($newArray);
            }
            $list = new room;
            $list->Ma_PB = strtoupper($string);
            $list->Ten_PB = $request->input('ten_pb');

            $list->save();   
            DB::commit();
            return Redirect()->route('room_index')->withSuccess('Thêm thành công','Item created successfully!');
        }

       catch(\Exception $exception)
        {
            return Redirect()->route('room_index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(room $room)
    {
        return view('room.edit_room',compact('room'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try
         {
            DB::beginTransaction();
                $ar = array('đ' => 'd','ủ' => 'u','Đ' => 'd','U' => 'u');
                $bien = strtr($request->ten_pb, $ar);
                $arr = explode(" ",$bien);
                $newArray = array(); 
                foreach($arr as $value) 
                { 
                    
                    $newArray[] = $value[0];
                    $string = implode($newArray);
                }
                $list = room::find($request->input('id'));
                $list->Ma_PB = strtoupper($string);
                $list->Ten_PB = $request->input('ten_pb');
                $list->save();
                
              
            DB::commit();
            return Redirect()->route('room_index')->withSuccess('Sửa Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('room_index')->witherror('Thêm Không Thành Công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(room $room)
    {
        
        $fk = DB::table('staff')->where('staff.id_room','=',$room->id)->get();
        if(empty($fk) == true)
        {
            $rooms = $this->room->find($room->id);
            $rooms->delete($room->id);
            return redirect()->route('room_index')->withSuccess('Thành công','Item created successfully!');
        }
        else
        {
           return redirect()->route('room_index')->witherror('Vẫn Còn Cán Bộ Trong Phòng Ban Này!','Item created successfully!');
        }
    }
    public function show_list(room $room)
    {
        $rooms = $this->room->find($room->id);
        $listnv = DB::table('staff')
                    ->join('room','room.id','=','staff.id_room')
                    ->where('staff.status','=',0)
                    ->where('room.id','=',$rooms->id)
                    ->select('Ma_NV','Ho_Ten','Ten_PB')->get();

        if($listnv->isEmpty())
        {
           return back()->witherror('Không Có Nhân Viên Thuộc Phòng Ban Này');
        }
        else
        {
           return view('room.list_nv', compact('listnv')); 
        }
    }
}
