<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\corporate_position;
use App\party_position;
use App\goverment_position;
use App\rank;
use App\family_information;
use App\major;
use App\personal_information;
use App\staff_bonus_discipline;
use App\room;
use App\degree;
use App\history_rank;
use App\history_goverment_position;
use App\history_party_position;
use App\history_corporate_position;
use App\User;
use DB;
use Hash;
use App\Exports\user_Export;
use Excel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use Carbon\Carbon;

class Controller_personal_information extends Controller
{
    private $personal_information;
    private $staff_bonus_discipline;
    private $history_rank;
    private $history_goverment_position;
    private $history_party_position;
    private $history_corporate_position;
    private $family_information;
    private $user;
    public function __construct(personal_information $personal_information,staff_bonus_discipline $staff_bonus_discipline,history_rank $history_rank,history_goverment_position $history_goverment_position,history_party_position $history_party_position,history_corporate_position $history_corporate_position,family_information $family_information,User $user)
    {
        $this->staff_bonus_discipline = $staff_bonus_discipline; 
        $this->personal_information = $personal_information;
        $this->history_rank = $history_rank;
        $this->history_corporate_position = $history_corporate_position;
        $this->history_goverment_position = $history_goverment_position;
        $this->history_party_position = $history_party_position;
        $this->family_information = $family_information; 
        $this->User = $user;
    }

    public function index()
    {
        $bien = Carbon::now()->year;
        $ranks = DB::table('rank')->get();
        $rooms = DB::table('room')->get();
        $degrees = DB::table('degree')->get(); 
        $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('staff.status','=',0)
         ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        $vanbang = DB::table('staff')->where('Van_Bang_Khac','!=',null)->get();
        $time = date('Y-m-d',strtotime(str_replace('/','-',Carbon::now())));
           return view('staff_information.personal_information.index',compact('personal_informations','bien','ranks','vanbang','rooms','degrees'));
         dd($personal_informations);
    }
    public function list_nv_tang_ham()
    {
        $lenham = DB::table('staff')
        ->join('rank','staff.rank_id','=','rank.id')
        ->where([
                    ['id_degree','=',1],
                    ['rank_id','<',12],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                    ['status','=',0],
                ])
                ->orwhere([
                    ['id_degree','=',2],
                    ['rank_id','<',15],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                    ['status','=',0],
                ])
                ->orwhere([
                    ['id_degree','=',3],
                    ['rank_id','<',19],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                    ['status','=',0],
                ])
                ->orwhere([
                    ['id_degree','=',4],
                    ['rank_id','<',27],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                    ['status','=',0],
                ])
                ->orwhere([
                    ['id_degree','=',5],
                    ['rank_id','<',35],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                    ['status','=',0],
                ])
                ->orwhere([
                    ['id_degree','=',6],
                    ['rank_id','<',43],
                    ['Ngay_Len_Ham','<=',Carbon::now()],
                    ['status','=',0],
                ])
        ->select('staff.id','rank_id','Ho_Ten','Ten_Cap_Bac','Cap_Bac_Nam_Sau','Ngay_Len_Ham',)->get();
        return view('staff_information.personal_information.nv_tangham_theonam',compact('lenham'));
    }
    public function show_ls_rank(personal_information $personal_information)
    {
        $lsrank = DB::table('history_rank')
        ->join('staff','staff.Ma_NV','=','history_rank.Ma_NV')
        ->join('rank','rank.id','=','id_rank')
        ->where('history_rank.Ma_NV','=',$personal_information->Ma_NV)
        ->where('staff.status','=',0)
        ->select('history_rank.id','history_rank.Ma_NV','Ho_Ten','LS_Ngay_Tang_Ham','id_rank','Ten_Cap_Bac')
        ->get();
        return view('staff_information.personal_information.history_rank_nv',compact('lsrank'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ranks = DB::table('rank')->get();
        $goverment_positions = DB::table('goverment_position')->get();
        $party_positions = DB::table('party_position')->get();
        $corporate_positions = DB::table('corporate_position')->get();
        $majors = DB::table('major')->get();
        $rooms = DB::table('room')->get();
        $degree = DB::table('degree')->get();
         return view('staff_information.personal_information.create',compact('ranks','goverment_positions','party_positions','corporate_positions','majors','rooms','degree'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_cuoi = DB::table('staff')->select('id')->get();
        $nam = getdate(strtotime(date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Vao_Lam'))))));
        $dt = date('Y-m-d',strtotime(date('Y-m-d',strtotime(Carbon::create($nam["year"],6,1,0)))."+1 YEAR")); 
         try
         {
             DB::beginTransaction();

             $list = new personal_information;
             $list1 = new history_rank;
             $list2 = new history_goverment_position;
             $list3 = new history_party_position;
             $list4 = new history_corporate_position;
             $list6 = new User;
             $list->Ho_Ten = $request->Ho_Ten;
             $list->Ma_NV = 'MANV0'.(count($id_cuoi) + 1);
             $list->Gioi_Tinh = $request->Gioi_Tinh;
             $list->Ngay_Sinh = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Sinh'))));
             $list->Noi_O_Hien_Tai =  $request->Noi_O_Hien_Tai;
             $list->Que_Quan = $request->Que_Quan;
             $list->Ton_Giao = $request->Ton_Giao;
             $list->Dan_Toc = $request->Dan_Toc;
             $list->Ly_Luan_Chinh_Tri = $request->Ly_Luan_Chinh_Tri;
             $list->id_degree = $request->Trinh_Do;
             $list->Ngoai_Ngu = $request->Ngoai_Ngu;
             $list->Tin_Hoc = $request->Tin_Hoc;
             $list->id_room = $request->room_id;
             $list->Ngay_Vao_Dang = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Vao_Dang'))));
             $list->Ngay_Vao_Doan = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Vao_Doan'))));
             $list->Ngay_Vao_Lam = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Vao_Lam'))));
             $list->Tinh_Trang_Lam_Viec = "Làm Việc";
             $list->status = 0;
             $list->goverment_position_id = $request->government_positions_id;
             $list->corporate_postion_id = $request->corporate_position_id;
             $list->party_postion_id = $request->party_position_id;
             $list->major_id = $request->major_id;
             $list->rank_id = $request->rank_id;
             $list->Luong_CB = $request->Luong_CB;
             $list->Van_Bang_Khac = $request->Van_Bang;
             $list->id_rank_DT = $request->rank_id;
             $list->Ngay_Len_Ham = $dt;
             $list->stt_ky_luat = 0;
             if($request->hasFile('Avatar')){
                 $file = $request->file('Avatar');
                 $extension = $file->getClientOriginalName();
                 $file->move('adminlte/dist/img/',$extension);
                 $list->Avatar = $extension;
             }
            $list1->Ma_NV = 'MANV0'.(count($id_cuoi) + 1);
            $list1->id_rank = $request->rank_id;
            $list1->LS_Ngay_Tang_Ham = Carbon::now();

            $list2->Ma_NV = 'MANV0'.(count($id_cuoi) + 1);
            $list2->id_goverment_position = $request->government_positions_id;
            $list2->Ngay_Nhan_Chuc = Carbon::now();

            $list3->Ma_NV = 'MANV0'.(count($id_cuoi) + 1);
            $list3->id_party_position = $request->party_position_id;
            $list3->Ngay_Nhan_Chuc = Carbon::now();

            $list4->Ma_NV = 'MANV0'.(count($id_cuoi) + 1);
            $list4->id_corporate_position = $request->corporate_position_id;
            $list4->Ngay_Nhan_Chuc = Carbon::now();

            $list5 = new family_information;
            $list5->Ho_Ten_Cha = $request->input('Ho_Ten_Cha');
            $list5->Nghe_Nghiep_Cha = $request->input('Nghe_Nghiep_Cha');
            $list5->Noi_Sinh_Cha = $request->input('Noi_Sinh_Cha');

            $list5->Ho_Ten_Me = $request->input('Ho_Ten_Me');
            $list5->Nghe_Nghiep_Me = $request->input('Nghe_Nghiep_Me');
            $list5->Noi_Sinh_Me = $request->input('Noi_Sinh_Me');

            $list5->Ho_Ten_Vo_Chong = $request->input('Ho_Ten_Vo_Chong');
            $list5->Nghe_Nghiep_Vo_Chong = $request->input('Nghe_Nghiep_Vo_Chong');
            $list5->Noi_Sinh_Vo_Chong = $request->input('Noi_Sinh_Vo_Chong');

            $list5->Ho_Ten_Con1= $request->input('Ho_Ten_Con1');
            $list5->Nghe_Nghiep_Con1 = $request->input('Nghe_Nghiep_Con1');
            $list5->Ho_Ten_Con2 = $request->input('Ho_Ten_Con2');
            $list5->Nghe_Nghiep_Con2 = $request->input('Nghe_Nghiep_Con2');
            $list5->Ho_Ten_Con3 = $request->input('Ho_Ten_Con3');
            $list5->Nghe_Nghiep_Con3 = $request->input('Nghe_Nghiep_Con3');

            $list5->id_staff = 'MANV0'.(count($id_cuoi) + 1);

            $list6->email = $request->email;
            $list6->name = $request->Ho_Ten;
            $list6->password = Hash::make('123456');
            $list6->Ma_NV_id = 'MANV0'.(count($id_cuoi) + 1);
            $list6->type = 'default';

            $list5->save();   

            $list1->save();
            $list->save();  
            $list2->save();
            $list3->save();
            $list4->save();
            $list6->save();
             DB::commit();
             return Redirect()->route('staff.create')->withSuccess('Thêm Thành Công','Item created successfully!');
         }

         catch(\Exception $exception)
         {
             return Redirect()->route('staff.create')->witherror('Thêm Không Thành Công!');
         }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(personal_information $personal_information)
    {
        // dd(date('d-m-Y',strtotime(str_replace('/','-',$personal_information->Ngay_Sinh))));
        $ranks = DB::table('rank')->get();
        $goverment_positions = DB::table('goverment_position')->get();
        $party_positions = DB::table('party_position')->get();
        $corporate_positions = DB::table('corporate_position')->get();
        $majors = DB::table('major')->get();
        $degrees = DB::table('degree')->get();
        $rooms = DB::table('room')->get();
        $r = DB::table('history_rank')->where('Ma_NV','=',$personal_information->Ma_NV)->count();
         return view('staff_information.personal_information.edit',compact('personal_information','ranks','goverment_positions','party_positions','corporate_positions','majors','degrees','rooms','r'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {  

        $r = DB::table('history_rank')->where('Ma_NV','=',$request->Ma_NV)->count();
        $g = DB::table('history_goverment_position')->where('Ma_NV','=',$request->Ma_NV)->count();
        $p = DB::table('history_party_position')->where('Ma_NV','=',$request->Ma_NV)->count();
        $c = DB::table('history_corporate_position')->where('Ma_NV','=',$request->Ma_NV)->count();
        try
        {
            DB::beginTransaction();

                $list = personal_information::find($request->input('id'));
                $list1 = history_rank::find($request->input('Ma_NV'));
                $list2 = history_goverment_position::find($request->input('Ma_NV'));
                $list3 = history_party_position::find($request->input('Ma_NV'));
                $list4 = history_corporate_position::find($request->input('Ma_NV'));
                $list->Ho_Ten = $request->Ho_Ten;
                $list->Gioi_Tinh = $request->Gioi_Tinh;
                $list->Ngay_Sinh = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Sinh'))));
                $list->Noi_O_Hien_Tai =  $request->Noi_O_Hien_Tai;
                $list->Que_Quan = $request->Que_Quan;
                $list->Ton_Giao = $request->Ton_Giao;
                $list->Dan_Toc = $request->Dan_Toc;
                $list->Ly_Luan_Chinh_Tri = $request->Ly_Luan_Chinh_Tri;
                $list->id_degree = $request->Trinh_Do;
                $list->Ngoai_Ngu = $request->Ngoai_Ngu;
                $list->Tin_Hoc = $request->Tin_Hoc;
                $list->Ngay_Vao_Dang = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Vao_Dang'))));
                $list->Ngay_Vao_Doan = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Vao_Doan'))));
                $list->Ngay_Vao_Lam = date('Y-m-d',strtotime(str_replace('/','-',$request->input('Ngay_Vao_Lam'))));
                $list->Tinh_Trang_Lam_Viec = $request->Tinh_Trang_Lam_Viec;
                $list->goverment_position_id = $request->government_positions_id;
                $list->corporate_postion_id = $request->corporate_position_id;
                $list->party_postion_id = $request->party_position_id;
                $list->major_id = $request->major_id;
                $list->rank_id = $request->rank_id;
                $list->id_rank_DT = $request->rank_id;
                if($request->hasFile('Avatar')){
                 $file = $request->file('Avatar');
                 $extension = $file->getClientOriginalName();
                 $file->move('adminlte/dist/img/',$extension);
                 $list->Avatar = $extension;
                }
                $list->Luong_CB = $request->Luong_CB;
                $list->Van_Bang_Khac = $request->Van_Bang;
                if(count($r) > 1 || count($g) > 1 || count($p) > 1 || count($c) > 1)
                {
                    return Redirect()->route('staff.index')->withError('Cán Bộ Đã Tồn Tại Lịch Sử');
                }
                if(count($r) <= 1)
                {
                    $list1->id_rank = $request->rank_id;
                    $list1->save();
                }
                
                if(count($g) <= 1)
                {
                    $list2->id_goverment_position = $request->government_positions_id;
                    $list2->save();
                }

                if(count($p) <= 1)
                {
                    $list3->id_party_position = $request->party_position_id;
                    $list3->save();
                }

                if(count($c) <= 1)
                {
                    $list4->id_corporate_position = $request->corporate_position_id;
                    $list4->save();
                }

                $list->save();  
                
                DB::commit();
                return Redirect()->route('staff.index')->withSuccess('Sửa Thông Tin Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('staff.index')->witherror('Sửa Không Thành Công!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(personal_information $personal_information)
    {
        $list = personal_information::find($personal_information->id);
        $list->status = 1;
        $list->save();
        return redirect()->route('staff.index')
            ->withSuccess('success','Item created successfully!');
    }

    public function find(request $request)
    {
        $bien = Carbon::now()->year;
        $ranks = DB::table('rank')->get();
        $vanbang = DB::table('staff')->where('Van_Bang_Khac','!=',null)->get();
        if(is_null($request->value))
        {
            $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
        ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        }
        else
        {
        $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('Ho_Ten','like',$request->value)
         ->orwhere('Que_Quan','like',$request->value)
         ->orwhere('Gioi_Tinh','like',$request->value)
         ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        }

        return view('staff_information.personal_information.index',compact('personal_informations','bien','ranks','vanbang'));
    }

    public function filter(Request $request)
    {
        $bien = Carbon::now()->year;
        $ranks = DB::table('rank')->get();
        $vanbang = DB::table('staff')->where('Van_Bang_Khac','!=',null)->get();
        if(is_null($request->cap_bac) && is_null($request->Ly_Luan_Chinh_Tri) && is_null($request->Trinh_Do))
        {
            $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
        ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        }
        elseif (is_null($request->cap_bac) && is_null($request->Ly_Luan_Chinh_Tri) && is_null($request->Trinh_Do) == false) 
        {
            $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('degree.id','=',$request->Trinh_Do)
        ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        }
        elseif (is_null($request->cap_bac) && is_null($request->Ly_Luan_Chinh_Tri)  == false && is_null($request->Trinh_Do)) 
        {
            $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('Ly_Luan_Chinh_Tri','=',$request->Ly_Luan_Chinh_Tri)
         ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
         
        }
        elseif (is_null($request->cap_bac)  == false && is_null($request->Ly_Luan_Chinh_Tri) && is_null($request->Trinh_Do)) 
        {
            $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('rank_id','=',$request->cap_bac)
         ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        }
        elseif (is_null($request->cap_bac)  == false && is_null($request->Ly_Luan_Chinh_Tri) == false && is_null($request->Trinh_Do)) 
        {
            $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('rank_id','=',$request->cap_bac)
         ->where('Ly_Luan_Chinh_Tri','=',$request->Ly_Luan_Chinh_Tri)
         ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        }
        elseif (is_null($request->cap_bac)  == false && is_null($request->Ly_Luan_Chinh_Tri)  && is_null($request->Trinh_Do) == false) 
        {
            $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('rank_id','=',$request->cap_bac)
         ->where('degree.id','=',$request->Trinh_Do)
         ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        }
        elseif (is_null($request->cap_bac)   && is_null($request->Ly_Luan_Chinh_Tri) == false  && is_null($request->Trinh_Do) == false) 
        {
            $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('Ly_Luan_Chinh_Tri','=',$request->Ly_Luan_Chinh_Tri)
         ->where('degree.id','=',$request->Trinh_Do)
         ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        }
        else
        {
            $personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->where('rank_id','=',$request->cap_bac)
         ->where('Ly_Luan_Chinh_Tri','=',$request->Ly_Luan_Chinh_Tri)
         ->where('degree.id','=',$request->Trinh_Do)
         ->select('staff.id','Avatar','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang','Ngay_Vao_Lam',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get();
        }
        if($personal_informations->isempty())
        {
            return view('staff_information.personal_information.index',compact('personal_informations','bien','ranks','vanbang'))->witherror('Thăng Hàm Không Thành Công!');;
        }
        else
        {
            return view('staff_information.personal_information.index',compact('personal_informations','bien','ranks','vanbang'));
        }
    }
    public function autocomplete(Request $request)
    {
        $data = personal_information::select("Ho_Ten as name")->where('staff.status','=',0)->where("Ho_Ten","LIKE","%{$request->input('value')}%")->get();
        return response()->json($data);
    }

    public function lenhamtheonam(personal_information $personal_information)
    {
        try
         {
            DB::beginTransaction();
            $bien = DB::table('staff')
                        ->where('staff.id','=',$personal_information->id)
                        ->select('rank_id')->get();
            $list = personal_information::find($personal_information->id);
            $list1 = new history_rank;
            if($bien[0]->rank_id == 43)
            {
                return Redirect()->route('staff.index')->withError('Không Thể Tăng Hàm Thêm nữa!!');
            }
            elseif($bien[0]->rank_id < 43 && $list->stt_ky_luat != 0)
            {
                $list->stt_ky_luat = $list->stt_ky_luat - 1;
                $list->save();
                DB::commit();
                return Redirect()->route('staff.lenham1')->withSuccess('Giảm Hình Thức Kỉ Luật','Item created successfully!');
            }
            else
            {   
                $list->Ngay_Len_Ham = date('Y-m-d',strtotime(date('Y-m-d',strtotime(str_replace('/','-',$personal_information->Ngay_Len_Ham)))."+1 YEAR"));
                $list->rank_id = $bien[0]->rank_id + 1;
                $list->id_rank_DT = $bien[0]->rank_id + 1;
                $list->save();
                $list1->Ma_NV = $personal_information->Ma_NV;
                $list1->id_rank = $personal_information->rank_id;
                $list1->LS_Ngay_Tang_Ham = $personal_information->Ngay_Len_Ham;
                $list1->save();
                
            } 
            DB::commit();
            return Redirect()->route('staff.lenham1')->withSuccess('Thăng Hàm Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('staff.lenham1')->witherror('Thăng Hàm Không Thành Công!');
        }
    }

    public function hien_hsl()
    {
        $dt = date('Y-m-d',strtotime(Carbon::create(Carbon::now()->year,6,1,0))); 
        $time = date('Y-m-d',strtotime(str_replace('/','-',Carbon::now())));
        $personal_informations = DB::table('staff')
                        ->join('rank','staff.id_rank_DT','=','rank.id')
                        ->where([
                            ['Ngay_Len_Ham','<=',Carbon::now()],
                            ['id_degree','=',1],
                            ['rank_id','>=',12],
                            ['status','=',0],
                        ])
                        ->orwhere([
                            ['Ngay_Len_Ham','<=',Carbon::now()],
                            ['id_degree','=',2],
                            ['rank_id','>=',15],
                            ['status','=',0],
                        ])
                        ->orwhere([
                            ['Ngay_Len_Ham','<=',Carbon::now()],
                            ['id_degree','=',3],
                            ['rank_id','>=',19],
                            ['status','=',0],
                        ])
                        ->orwhere([
                            ['Ngay_Len_Ham','<=',Carbon::now()],
                            ['id_degree','=',4],
                            ['rank_id','>=',27],
                            ['status','=',0],
                        ])
                        ->orwhere([
                            ['Ngay_Len_Ham','<=',Carbon::now()],
                            ['id_degree','=',5],
                            ['rank_id','>=',35],
                            ['status','=',0],
                        ])
                        ->orwhere([
                            ['Ngay_Len_Ham','<=',Carbon::now()],
                            ['id_degree','=',6],
                            ['status','=',0],
                        ])
                        ->select('staff.id','He_So_Luong','Ho_Ten','Ngay_Len_Ham')->get();
        return view('staff_information.personal_information.nv_tang_hsl',compact('personal_informations'));
        
    }
    
    public function nang_hsl(personal_information $personal_information)
    {
        try
         {
            DB::beginTransaction();
            $bien = DB::table('staff')
                        ->where('staff.id','=',$personal_information->id)
                        ->select('id_rank_DT')->get();
            $list = personal_information::find($personal_information->id);
            $bienmoi = $bien[0]->id_rank_DT + 1;
            if($bien[0]->id_rank_DT < 43 && $list->stt_ky_luat != 0)
            {
                 $list->stt_ky_luat = $list->stt_ky_luat - 1;
                 $list->save();
            }
            else
            {   
                $list->Ngay_Len_Ham = date('Y-m-d',strtotime(date('Y-m-d',strtotime(str_replace('/','-',$personal_information->Ngay_Len_Ham)))."+1 YEAR"));
                $list->id_rank_DT = $bienmoi;
                $list->save();
            } 
            DB::commit();
            return Redirect()->route('staff.hsl')->withSuccess('Thăng Hàm Thành Công','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            return Redirect()->route('staff.hsl')->witherror('Thăng Hàm Không Thành Công!');
        }
    }
    public function show_history_tangham()
    {
        $personal_informations = DB::table('staff')->where('staff.status','=',0)->select('Ma_NV','Ho_Ten')->get();
        return view('staff_information.personal_information.history_nv_tang_ham',compact('personal_informations'));
    }
    public function find_ls_nv(Request $request)
    {
        $personal_informations = DB::table('staff')->select('Ma_NV','Ho_Ten')->get();
        $rank = DB::table('history_rank')
        ->join('rank','rank.id','=','id_rank')
        ->where('Ma_NV','=',$request->Ho_Ten)->get();

        $goverment_position = DB::table('history_goverment_position')
        ->join('goverment_position','goverment_position.id','=','id_goverment_position')
        ->where('Ma_NV','=',$request->Ho_Ten)->get();

        $party_position = DB::table('history_party_position')
        ->join('party_position','party_position.id','=','id_party_position')
        ->where('Ma_NV','=',$request->Ho_Ten)->get();

        $corporate_position = DB::table('history_corporate_position')
        ->join('corporate_position','corporate_position.id','=','id_corporate_position')
        ->where('Ma_NV','=',$request->Ho_Ten)
        ->get();

        $list = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->where('Ma_NV','=',$request->Ho_Ten)
         ->select('staff.id','Avatar','Ho_Ten','TenChucvuCQ','TenChucvuDoan','Ten_Cap_Bac','TenChucvuDang')
         ->get();

          return view('staff_information.personal_information.history_nv_tang_ham',compact('rank','goverment_position','party_position','corporate_position','personal_informations','list'));    
    }
    public function export() 
    {
        
        return Excel::download(new user_Export , 'invoices.xlsx');
    }
}
