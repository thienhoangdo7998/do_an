<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Views;
use App\role;
use App\permission;


class ControllerRole extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $role;
    private $permission;
    public function __construct(role $roles,permission $permissions)
    {
        $this->role = $roles; 
        $this->permission = $permissions;
    }

    public function index()
    {
        $roles = $this->role->all();
       
        return view('role.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = $this->permission->all();
        return view('role.create_role',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $list = new role;
            $list->name = $request->input('name');
            $list->display_name = $request->input('display_name');
            $list->save();   
            $list->Premission()->attach($request->permission);
            DB::commit();

            return Redirect()->route('Role.index')->withSuccess('success','Item created successfully!');
        }

        catch(\Exception $exception)
        {
            DB::rollback();
        }   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(role $role)
    {
        $permissions = $this->permission->all();
        $listpermissions = DB::table('permission_role')->where('id_role', $role->id )->pluck('id_permission');
        return view('role.edit_role',compact('role','permissions','listpermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try
        {
            DB::beginTransaction();
                $list = role::find($request->input('id'));
                $list->name = $request->input('name');
                $list->display_name = $request->input('display_name');
                $list->save();
        
                DB::table('permission_role')->where('id_role',$request->id)->delete();
        
                 $list->Premission()->attach($request->permission);
      
            DB::commit();
            return Redirect()->route('Role.index')->withSuccess('success','Item created successfully!');
        }
        catch(\Exception $exception)
        {
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(role $role)
    {
        $roles = $this->role->find($role->id);
        $roles->delete($roles->id);
        $roles->Premission()->detach();
        return redirect()->route('Role.index')
            ->withSuccess('success','Item created successfully!');
    }
}
