<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    protected $table = 'role';
    protected $fillable = [
    'id',
    'id_user',
    'id_role',
    ];
    public function Premission()
    {
        return $this->belongsToMany('App\permission','permission_role','id_role', 'id_permission');
    }
}
