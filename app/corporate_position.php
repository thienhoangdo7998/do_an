<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class corporate_position extends Model
{
    protected $table = 'corporate_position';
    protected $fillable = [
      	'id',
      	'Ma_CVD',
      	'TenChucvuDoan',
      ];
}
