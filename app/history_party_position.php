<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history_party_position extends Model
{
    protected $table = 'history_party_position';
    protected $fillable = [
      	'id',
      	'Ma_NV',
      	'Ngay_Nhan_Chuc',
      	'id_party_position',
      ];
}
