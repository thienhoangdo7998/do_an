<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class goverment_position extends Model
{
    protected $table = 'goverment_position';
    protected $fillable = [
      	'id',
      	'Ma_CVCQ',
      	'TenChucvuCQ',
      ];
}
