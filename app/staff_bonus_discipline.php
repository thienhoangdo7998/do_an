<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class staff_bonus_discipline extends Model
{
    protected $table = 'staff_bonus_discipline';
    protected $fillable = [
    'id',
    'Ly_Do',
    'Ngay_QD',
    'id_staff',
    'id_bonus_discipline',
    'is_disable',
    ];
}
