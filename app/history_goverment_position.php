<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history_goverment_position extends Model
{
    protected $table = 'history_goverment_position';
    protected $fillable = [
      	'id',
      	'Ma_NV',
      	'Ngay_Nhan_Chuc',
      	'id_goverment_position',
      ];
}
