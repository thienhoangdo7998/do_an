<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rank extends Model
{
    protected $table = 'rank';
    protected $fillable = [
      	'id',
      	'Ten_Cap_Bac',
      	'He_So_Luong',
      	'Cap_Bac_Nam_Sau',
      	'Nam_Len_Ham',
      ];
}
