<?php

namespace App\Exports;

use App\User;
use App\role;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;



class user_Export implements FromArray,WithHeadings,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */


    public function array(): array
    {
    	$personal_informations = DB::table('staff')
         ->join('corporate_position','staff.corporate_postion_id','=','corporate_position.id')
         ->join('party_position','staff.party_postion_id','=','party_position.id')
         ->join('goverment_position','staff.goverment_position_id','=','goverment_position.id')
         ->join('rank','staff.rank_id','=','rank.id')
         ->join('major','staff.major_id','=','major.id')
         ->join('degree','staff.id_degree','=','degree.id')
         ->where('staff.status','=',0)
         ->select('staff.Ma_NV','Ho_Ten','Gioi_Tinh','Ngay_Sinh','Que_Quan','Dan_Toc','Ton_Giao','Noi_O_Hien_Tai','Ly_Luan_Chinh_Tri','Ngoai_Ngu','Tin_Hoc','Ngay_Vao_Doan','Ngay_Vao_Dang','Tinh_Trang_Lam_Viec','Ngay_Vao_Lam','TenChucvuCQ','TenChucvuDoan','Ten_trinh_do','Ten_Cap_Bac','Van_Bang_Khac','Ten_Nghiep_Vu','TenChucvuDang',
           DB::raw('YEAR(Ngay_Vao_Dang) as Tuoi_Dang'))
         ->get()->toarray();
        return $personal_informations;
    }
    public function headings(): array
    {
    	return[
    		'Mã Nhân Viên',
    		'Họ Tên',
    		'Giới Tính',
    		'Ngày Sinh',
    		'Quê Quán',
    		'Dân Tộc',
    		'Tôn Giáo',
    		'Nơi Ở Hiện Tại',
    		'Lý Luận Chính Trị',
    		'Ngoại Ngữ',
    		'Tin Học',
    		'Ngày Vào Đoàn',
    		'Ngày Vào Đảng',
    		'Tính Trạng Làm Việc',
    		'Ngày Vào Làm',
    		'Tên Chức Vụ Chính Quyền',
    		'Tên Chức Vụ Đoàn',
    		'Tên Trình Độ',
    		'Tên Cấp Bậc',
    		'Văn Bằng Khác',
    		'Tên Nghiệp Vụ',
    		'Tên Chức Vụ Đảng',
    		'Tuổi Đảng',
    	];
    }

}
