<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history_rank extends Model
{
    protected $table = 'history_rank';
    protected $fillable = [
      	'id',
      	'Ma_NV',
      	'LS_Ngay_Tang_Ham',
      	'id_rank',
      ];
}
