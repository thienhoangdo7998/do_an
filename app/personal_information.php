<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class personal_information extends Model
{
     protected $table = 'staff';
     protected $fillable = [
      	'id',
            'Ma_NV',
      	'Ho_Ten',
      	'Gioi_Tinh',
      	'Ngay_Sinh',
      	'Que_Quan',
      	'Dan_Toc',
      	'Ton_Giao',
      	'Noi_O_Hien_Tai',
      	'Ly_Luan_Chinh_Tri',
      	'Ngoai_Ngu',
      	'Tin_Hoc',
      	'Ngay_Vao_Doan',
      	'Ngay_Vao_Dang',
            'Ngay_Vao_Lam',
      	'Tinh_Trang_Lam_Viec',
      	'Ngay_Vao_Lam',
            'Van_Bang_Khac',
      	'corporate_postion_id',
      	'government_position_id',
      	'party_postion_id',
      	'rank_id',
      	'major_id',
            'Avatar',
            'Luong_CB',
            'Ngay_Len_Ham',
            'id_room',
            'id_rank_DT',
            'stt_ky_luat',
            'status'
      ];
}
