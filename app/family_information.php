<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class family_information extends Model
{
    protected $table = 'family_information';
    protected $fillable = [
      	'id',
	  	'Ho_Ten_Cha',
	    'Ho_Ten_Me',
        'Nghe_Nghiep_Cha',
        'Nghe_Nghiep_Me',
        'Noi_Sinh_Cha',
        'Noi_Sinh_Me',
        'Ho_Ten_Vo_Chong',
        'Nghe_Nghiep_Vo_Chong',
        'Noi_Sinh_Vo_Chong',
        'Ho_Ten_Con1',
        'Nghe_Nghiep_Con1',
        'Ho_Ten_Con2',
        'Nghe_Nghiep_Con2',
        'Ho_Ten_Con3',
        'Nghe_Nghiep_Con3',
        'id_staff',
	  ];
}
