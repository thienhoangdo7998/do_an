<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class room extends Model
{
    protected $table = 'room';
    protected $fillable = [
    'id',
    'Ma_PB',
    'Ten_PB',
    ];
}
