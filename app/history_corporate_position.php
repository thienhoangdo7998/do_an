<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history_corporate_position extends Model
{
    protected $table = 'history_corporate_position';
    protected $fillable = [
      	'id',
      	'Ma_NV',
      	'Ngay_Nhan_Chuc',
      	'id_corporate_position',
      ];
}
