<?php

use Illuminate\Support\Facades\Route;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['middleware' => 'guest'],function(){
	Route::match(['get','post'],'/login',['as' => 'login', 'uses' => 'loginController@login']);
	Route::get('/register',['as' => 'register','uses' => 'registerController@getregister']);
	Route::post('/register',['as' => 'register','uses' => 'registerController@postregister']);

	Route::get('/forgot-password',['as' => 'forgotpassword','uses' => 'registerController@forgot_password']);
	Route::post('/forgot-password',['as' => 'forgotpassword','uses' => 'registerController@postforgot_password']);

	Route::post('/change_password',['as' => 'change_password','uses' => 'registerController@change_password']);

});

Route::group(['middleware' => 'auth','isAdmin'],function(){
	Route::get('/home/{email}',['as' => 'home','uses' => 'loginController@index']);


	Route::prefix('/')->group(function(){
		Route::get('/', ['as' => 'home_page','uses' => 'Controller_trangchu@index']);
		Route::get('/test',function(){
			return view('layout.reset_password');
		});
	});


	Route::prefix('user')->group(function(){
		Route::get('/index', ['as' => 'user','uses' => 'UserController@index','middleware' => 'check:user-list']);
		Route::get('/index/{user}', ['as' => 'delete_user','uses' => 'UserController@destroy','middleware' => 'check:user-delete']);
		
		Route::get('/edit/{user}', ['as' => 'edit','uses' => 'UserController@edit','middleware' => 'check:user-edit']);
		Route::post('/edit/{user}', ['as' => 'edit','uses' => 'UserController@update']);

		Route::get('/create', ['as' => 'create','uses' => 'UserController@create','middleware' => 'check:user-create']);
		Route::post('/create', ['as' => 'create','uses' => 'UserController@store']);

		Route::post('/import_excel',['as'=>'import_execl','uses'=>'UserController@import']);
	});
	

	Route::prefix('roles')->group(function(){
		Route::get('/index', ['as' => 'Role.index','uses' => 'ControllerRole@index','middleware' => 'check:role-list']);

		Route::get('/index/{role}', ['as' => 'delete_role','uses' => 'ControllerRole@destroy','middleware' => 'check:role-delete']);
		Route::get('/edit/{role}', ['as' => 'edit_role','uses' => 'ControllerRole@edit','middleware' => 'check:role-edit']);
		Route::post('/edit/{role}', ['as' => 'edit_role','uses' => 'ControllerRole@update']);

		Route::get('/create', ['as' => 'create_role','uses' => 'ControllerRole@create','middleware' => 'check:role-create']);

		Route::post('/create', ['as' => 'create_role','uses' => 'ControllerRole@store']);
	});
	
	Route::prefix('party_position')->group(function(){
		Route::get('/index',['as' => 'party_position.index','uses' => 'Controllerparty_position@index']);
		Route::get('/create',['as' => 'party_position.create','uses' => 'Controllerparty_position@create']);

		Route::post('/create',['as' => 'party_position.create','uses' => 'Controllerparty_position@store']);

		Route::get('/edit/{party_position}', ['as' => 'party_position.edit','uses' => 'Controllerparty_position@edit']);

		Route::post('/edit/{party_position}', ['as' => 'party_position.edit','uses' => 'Controllerparty_position@update']);

		Route::get('/delete/{party_position}',['as' => 'party_position.delete','uses' => 'Controllerparty_position@destroy']);
		
		Route::get('/index/{party_position}',['as' => 'party_position.listnv','uses' => 'Controllerparty_position@listnv']);
	});

	Route::prefix('goverment_position')->group(function(){
		Route::get('/index',['as' => 'goverment_position.index','uses' => 'Controllergoverment_position@index']);
		Route::get('/create',['as' => 'goverment_position.create','uses' => 'Controllergoverment_position@create']);

		Route::post('/create',['as' => 'goverment_position.create','uses' => 'Controllergoverment_position@store']);

		Route::get('/edit/{goverment_position}', ['as' => 'goverment_position.edit','uses' => 'Controllergoverment_position@edit']);

		Route::post('/edit/{goverment_position}', ['as' => 'goverment_position.edit','uses' => 'Controllergoverment_position@update']);

		Route::get('/delete/{goverment_position}',['as' => 'goverment_position.delete','uses' => 'Controllergoverment_position@destroy']);

		Route::get('/index/{goverment_position}',['as' => 'goverment_position.listnv','uses' => 'Controllergoverment_position@listnv']);

		 Route::get('/update_goverment',['as' => 'goverment_position.update_goverment','uses' => 'Controllergoverment_position@update_goverment']);

		 Route::post('/update_goverment',['as' => 'goverment_position.update_goverment','uses' => 'Controllergoverment_position@post_update_goverment']);
	});

	Route::prefix('corporate_position')->group(function(){
		Route::get('/index',['as' => 'corporate_position.index','uses' => 'Controllercorporate_position@index']);
		 Route::get('/create',['as' => 'corporate_position.create','uses' => 'Controllercorporate_position@create']);

		 Route::post('/create',['as' => 'corporate_position.create','uses' => 'Controllercorporate_position@store']);

		 Route::get('/edit/{corporate_position}', ['as' => 'corporate_position.edit','uses' => 'Controllercorporate_position@edit']);

		 Route::post('/edit/{corporate_position}', ['as' => 'corporate_position.edit','uses' => 'Controllercorporate_position@update']);

		 Route::get('/delete/{corporate_position}',['as' => 'corporate_position.delete','uses' => 'Controllercorporate_position@destroy']);
		 
		  Route::get('/index/{corporate_position}',['as' => 'corporate_position.listnv','uses' => 'Controllercorporate_position@listnv']);

		  Route::get('/update_corporate',['as' => 'corporate_position.update_corporate','uses' => 'Controllercorporate_position@update_corporate']);

		  Route::post('/update_corporate',['as' => 'corporate_position.update_corporate','uses' => 'Controllercorporate_position@post_update_corporate']);
	});


	Route::prefix('major')->group(function(){
		Route::get('/index',['as' => 'major.index','uses' => 'Controllermajor@index']);
		 Route::get('/create',['as' => 'major.create','uses' => 'Controllermajor@create']);

		 Route::post('/create',['as' => 'major.create','uses' => 'Controllermajor@store']);

		 Route::get('/edit/{major}', ['as' => 'major.edit','uses' => 'Controllermajor@edit']);

		 Route::post('/edit/{major}', ['as' => 'major.edit','uses' => 'Controllermajor@update']);

		 Route::get('/delete/{major}',['as' => 'major.delete','uses' => 'Controllermajor@destroy']);
	});

	Route::prefix('rank')->group(function(){
		Route::get('/index',['as' => 'rank.index','uses' => 'Controllerrank@index']);
		Route::get('/create',['as' => 'rank.create','uses' => 'Controllerrank@create']);

		Route::post('/create',['as' => 'rank.create','uses' => 'Controllerrank@store']);

		Route::get('/edit/{rank}', ['as' => 'rank.edit','uses' => 'Controllerrank@edit']);

		Route::post('/edit/{rank}', ['as' => 'rank.edit','uses' => 'Controllerrank@update']);

		Route::get('/delete/{rank}',['as' => 'rank.delete','uses' => 'Controllerrank@destroy']);
	});

	Route::prefix('/staff')->group(function(){
		Route::get('/index',['as' => 'staff.index','uses' => 'Controller_personal_information@index','middleware' => 'check:staff-list']);

		Route::get('/create',['as' => 'staff.create','uses' => 'Controller_personal_information@create','middleware' => 'check:staff-add']);

		Route::post('/create',['as' => 'staff.create','uses' => 'Controller_personal_information@store']);

		Route::get('/edit/{personal_information}', ['as' => 'staff.edit','uses' => 'Controller_personal_information@edit','middleware' => 'check:staff-edit']);

		Route::post('/edit/{personal_information}', ['as' => 'staff.edit','uses' => 'Controller_personal_information@update']);

		Route::get('/delete/{personal_information}',['as' => 'staff.delete','uses' => 'Controller_personal_information@destroy','middleware' => 'check:staff-delete']);

		Route::post('/index',array('as'=>'staff.find','uses'=>'Controller_personal_information@find'));

		Route::post('/index/filter',['as'=>'staff.filter','uses'=>'Controller_personal_information@filter']);

		Route::get('/autocomplete',array('as'=>'staff.autocomplete','uses'=>'Controller_personal_information@autocomplete'));

		Route::get('/index/{staff_bonus_discipline}', ['as' => 'staff.level_up','uses' => 'Controller_personal_information@level_up']);

		Route::get('/lenham/{personal_information}', ['as' => 'staff.lenham','uses' => 'Controller_personal_information@lenhamtheonam']);

		Route::get('/lichsurank/{personal_information}', ['as' => 'staff.lsrank','uses' => 'Controller_personal_information@show_ls_rank']);

		Route::get('/lenham', ['as' => 'staff.lenham1','uses' => 'Controller_personal_information@list_nv_tang_ham']);

		Route::get('/list_nv/hsl',['as' => 'staff.hsl','uses' => 'Controller_personal_information@hien_hsl']);


		Route::get('/list_nv/hsl/{personal_information}',['as' => 'staff.hsl1','uses' => 'Controller_personal_information@nang_hsl']);

		Route::get('/lichsutangham',['as' => 'staff.history_tangham','uses' => 'Controller_personal_information@show_history_tangham']);

		Route::post('/lichsutangham',['as' => 'staff.find_ls_nv','uses' => 'Controller_personal_information@find_ls_nv']);

		Route::get('/excel',['as' => 'xuat_excel', 'uses' => 'Controller_personal_information@export']);
	});

	Route::prefix('room')->group(function(){
		Route::get('/index', ['as' => 'room_index','uses' => 'Controllerroom@index']);

		Route::get('/delete/{room}', ['as' => 'delete_room','uses' => 'Controllerroom@destroy']);

		Route::get('/edit/{room}', ['as' => 'edit_room','uses' => 'Controllerroom@edit']);

		Route::post('/edit/{room}', ['as' => 'edit_room','uses' => 'Controllerroom@update']);

		Route::get('/create', ['as' => 'create_room','uses' => 'Controllerroom@create']);

		Route::post('/create', ['as' => 'create_room','uses' => 'Controllerroom@store']);

		Route::get('/index/{room}',['as' => 'room_listnv','uses' => 'Controllerroom@show_list']);
	});
	

	Route::prefix('/family_information')->group(function(){
		Route::get('/index',['as' => 'family_information.index','uses' => 'ControllerFamily_information@index','middleware' => 'check:family-list']);
		Route::get('/create',['as' => 'family_information.create','uses' => 'ControllerFamily_information@create']);

		Route::post('/create',['as' => 'family_information.create','uses' => 'ControllerFamily_information@store']);

		Route::get('/edit/{family_information}', ['as' => 'family_information.edit','uses' => 'ControllerFamily_information@edit','middleware' => 'check:family-edit']);

		Route::post('/edit/{family_information}', ['as' => 'family_information.edit','uses' => 'ControllerFamily_information@update']);

		Route::get('/delete/{family_information}',['as' => 'family_information.delete','uses' => 'ControllerFamily_information@destroy','middleware' => 'check:family-delete']);
	});


	Route::prefix('/staff_bonus_discipline')->group(function(){
		Route::get('/index',['as' => 'staff_bonus_discipline.index','uses' => 'Controllerstaff_bonus_discipline@index','middleware' => 'check:staff-bonus-discipline-list']);
		Route::get('/create',['as' => 'staff_bonus_discipline.create','uses' => 'Controllerstaff_bonus_discipline@create']);

		Route::post('/create',['as' => 'staff_bonus_discipline.create','uses' => 'Controllerstaff_bonus_discipline@store']);

		Route::get('/edit/{staff_bonus_discipline}', ['as' => 'staff_bonus_discipline.edit','uses' => 'Controllerstaff_bonus_discipline@edit']);

		Route::post('/edit/{staff_bonus_discipline}', ['as' => 'staff_bonus_discipline.edit','uses' => 'Controllerstaff_bonus_discipline@update']);

		Route::get('/delete/{staff_bonus_discipline}',['as' => 'staff_bonus_discipline.delete','uses' => 'Controllerstaff_bonus_discipline@destroy']);

		Route::get('/staff_index', ['as' => 'staff_bonus_discipline.staff_index','uses' => 'Controllerstaff_bonus_discipline@index_staff']);

		Route::get('/list_nv_kt_kl', ['as' => 'staff_bonus_discipline.show_level_up','uses' => 'Controllerstaff_bonus_discipline@index_list_nv','middleware' => 'check:staff-bonus-discipline-list-check']);

		Route::get('/list_nv_kt/{staff_bonus_discipline}', ['as' => 'staff_bonus_discipline.level_up','uses' => 'Controllerstaff_bonus_discipline@level_up']);

		Route::get('/list_nv_kt_kl/{staff_bonus_discipline}', ['as' => 'staff_bonus_discipline.cancel_level_up','uses' => 'Controllerstaff_bonus_discipline@not_level_up']);

	});

	Route::get('/excel',['as' => 'excel', 'uses' => 'UserController@export']);

	

	Route::get('/staff',function(){
		return view('staff_information.personal_information.create');
	});
	
	Route::get('/logout',['as' => 'logout', 'uses' => 'loginController@logout']);
});